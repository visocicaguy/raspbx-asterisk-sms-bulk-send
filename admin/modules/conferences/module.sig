-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1516042544.3524
[hashes]
Conferences.class.php = ce3d210c7c5a695324d92770b44dafecf0441354d9f18b39a81eea88c909557d
LICENSE = 8ceb4b9ee5adedde47b31e975c1d90c73ad27b6b165a1dcd80c7c545eb65b903
README.md = b5d2c0201029edea5d38b11486d9bb7fd7afa037ab5fe5336c60a7094e781c9d
assets/js/conferences.js = f82b9e52e4703fbec2f58618c719fd7cc7d1263f876013aa92a4c8344c749a20
assets/less/conferences.less = 1c7ec5fd520fc1ab099c79c9fa6e15afb8e88dbd859e73b1f0e316d3d48d54ac
etc/confbridge.conf = 0627666fb0c414d84f1abc6a67c2770ee8fc8fe76ea460bb4b68e9dfff23d268
functions.inc.php = ec3f29d1e75bd55ab4eaee0196ab293256c918ad51002b063fc2bc24c5839d11
install.php = dec91d50f0068addcd7feb9962970beb334fb41c1c726e469642fa0d198e706c
module.xml = dd11fc255b5e63920b5f3e79ee189ab5cd099d380ab192f7e089d322e4b1193d
page.conferences.php = 1b6b36d49114e990517a1d9eb4495400cc66569b80540e0ca4101f143f012931
uninstall.sql = 61406fef1b549f2bb5f733b601abec87067d956d2f5ede776c492b5e53eca7cc
views/form.php = 8ac66544f13c774d4381c1d6d1a6ef17ee79a99adfb1d70826b8e9701038619b
views/grid.php = 2f66db2263ae4db84bde495e10de073f5cf052043f74a8673255ce4071eae96a
views/rnav.php = 92713ad32d5c127e6331eea4c547cbf4fd859b6171ec0c165c5186ae5597f26c
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJaXPkwAAoJEIbOh3Rp0urZB4sQAIY944yOwiyuKcaizz/1+eCt
2AIFMcGhaLMwS5o0TVXTOw0idG++QH/MibP3uUGPQpRrdlcu3cmeH3bYW0bdrcvz
cX2TYHoLBCYo4dAaGjLerRMYYvFU0q2c4yWXQXNIsE5FicvmxORWC0AEJ42oZOtm
3tJO7CRIGeJt0bz/5lYoc6t7rNN+DPgiYTtANqB5unXpmz5sQG3pofy6LBLCXf+J
sFO10SRuw+VbKJG3FDs3auTq0HQhCIG/GWaXOCmugPsmZlVsCQOg0edR+G3NSXD7
51Zh8YK8UnIbRkgHn+aRN2X1iCGC24CSMbV1tpZ3Ljak+pXyO+gXMdkqBLnvgCpV
DzsDJuw9BPd4ChL0dmy/KbXWVtPI6qGFASWexK8Pr1VeY/BvwybvHxRoLZ2D4eDO
uygnMkRY6PJ+AMOhcApbW7K0xyKVhd5W1DwW4WgWlqj9RKQCk3B7LtahgVvAifui
0qxzq9oGw5GQCXDbypToN2MUk/wydeaY/RtKtYL6KK5NZXNWkbB6/Ij+wjvt+XBt
8ZHZr9qS6D0Gm5/sDBPAuKwRuQiHCMV3G/I6Xnum7OsZpluDJT6jXBwETKv+yspC
uojxxypQgw4G/o58ld5+GkGgdYoh/4UgGIQxSVu9ICOTPKhSSIsqTt0yK+gw4tYk
cMlg6l/ZpoICcXylDPWP
=OBYu
-----END PGP SIGNATURE-----
