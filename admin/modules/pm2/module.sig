-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1513633159.1592
[hashes]
Console/Pm2.class.php = ee5fc5717a77a99f5c7d6e9818d7d0b8db8d69ea7b501508d07ff1b131e103f0
LICENSE = 57c8ff33c9c0cfc3ef00e650a1cc910d7ee479a8bc509f6c9209a7c2a11399d6
Pm2.class.php = fae377605fd127db0d0d2669a34a6dad83fe32c068ee6ac631eb349859873897
README.md = 166c482b8e60965cc11e2f5db78d83c3eedaee6e262fbdc4984503f9df807dc7
module.xml = 60c3a05ed73f127c18cd2038397c42d142797df97998be59e5c000b68fd210a1
node/package.json = cb048651bf0804131ad19d8730f07838b5ca62888f853be4791fed99b545c94e
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJaODWHAAoJEIbOh3Rp0urZMl8QAK/nDlBtlYnnw5C2nU64dHLc
EnnyfeBv+NoaUuSOogqN+0QKMXsSBHqxzHzz9TpjB8aU1Jj+sPQNdSJgirfx8Oyj
d9pHoKhcOXQOaTCkRHE85FQJGbVwu7j9EJG5Tb5Z0UtAlgjl+EgUk/wR//FFDQ6V
un5UDvJacA4kV0Y5ZkfjOF3NG3RowN+T+8igMGvHPiXTkb/XVJMAxLdxX7JbUVo4
RkahIIFIQQ674rg0A8kP+AA54iVHQka06JyX9hLZ+2h2gIXr2uT7NtN1wWiVv2Db
LTjzy9DidA1K9Z7Kxj5tUP1ltUG0Gjop47dnKtZVgQ0Gu5xcOE7qZ6c5AL6mwVKd
hMjizjgQA6V+QbXr9QtsPKoMXx56uY8U+d3kqlrf6NfFRHLvaNgS8SP/rynnGRYE
bKvbgU2/EHSflffRHkzYPn1FWge+NE0RQG+hY6k6a8XY7dAeUQKU+sG4VbgNVrXt
rTkW7rzTIQ4rmjaVuaw94BiKWC3qPiYY2shegbHPHm7Mzm+E2kuAKFuXWMF9a+0U
DapgTUCpU4daOV/CBc+0IiTXv6rCirVIG9BCpvqi+AH9ShuNJzxKCuvDXRt94xJF
vNEzaW1tszxAlMqFLlktROseMFJLBC2n4l/4n584pGBi0a1o/3kTINY2C1tK+VdT
fgv98tObkKNO6NYLzREy
=aoYR
-----END PGP SIGNATURE-----
