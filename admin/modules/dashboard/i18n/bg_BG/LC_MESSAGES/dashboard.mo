��    ;      �  O   �           	  	          
   #  	   .  O   8     �     �     �     �     �     �     �     �     �     �     �                          !     )     B  3   K  u        �                 
   #     .     6     U     \     j     {     �  8   �     �     �  
   �     �     �                         !     (     0     6     =     D     L     Q     W     \    b     t
     �
     �
     �
     �
  �   �
     C     T  3   c     �     �     �     �     �     �     �       #   1  
   U  
   `     k  
   q  *   |     �  [   �  �     ,   �               7     Q     f  I   �     �  +   �  ,   	  A   6  &   x  a   �       -        ?     S     b          �     �     �     �     �  
   �     �     �     �     �     �                  &   0   :      *   8   /       7            3   6                                             5   2      ,   
   '   .         #      4              9                     $                          !                     %             (      )   1   	      ;   "         -      +    %s ago 0 seconds 1 Minute 15 Minutes 5 Minutes <strong>'%s'</strong><br><i>(You can change this name in Advanced Settings)</i> Asterisk Blogs Critical Errors found Day Delete This Disk Frobulating Hour Ignore This Invalid Selection Invalid period Load Averages Memory Month MySQL Network No critical issues found Overview Please check for errors in the notification section Provides a system information dashboard, showing information about Calls, CPU, Memory, Disks, Network, and processes. Security Issue Show All Show New Since Reload Statistics Summary SysInfo updated %s seconds ago System System Alerts System Dashboard System Last Rebooted System Overview This is a critical issue and should be resolved urgently Uptime Warnings Found Web Server Week Welcome to %s day days hour hours minute minutes month months second seconds week weeks year years Project-Id-Version: FreePBX v2.6
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-01-28 19:38-0300
PO-Revision-Date: 2014-07-23 16:31+0200
Last-Translator: Chavdar <chavdar_75@yahoo.com>
Language-Team: Bulgarian <http://git.freepbx.org/projects/freepbx/dashboard/bg/>
Language: bg_BG
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 1.10-dev
X-Poedit-Language: Bulgarian
X-Poedit-Country: BULGARIA
X-Poedit-SourceCharset: utf-8
 преди %s 0 секунди 1 Минута 15 Минути 5 Минути <strong>'%s'</strong><br><i>(Можете да промените името в  Допълнителни Настройки)</i> Астериск Блогове Намерени са Критични Грешки Ден Изтрий Това Диск Промяна Час Игнорирайте Това Невалиден Избор Невалиден период Средно Натоварване Памет Месец MySQL Мрежа Няма критични проблеми Преглед Моля проверете за грешки в раздела за уведомления Предоставя информация за системата, показва информация за Разговори, CPU, Памет, Твърд Диск, Мрежа и процеси. Проблем със Сигурността Покажи Всички Покажи Новите От Обновяване Статистики Обща Информация Информацията е обновена преди %s секунди Система Системи Предупреждения Информация за Системата Последно Рестартиране на Системата Преглед на Системата Това е критичен проблем и трябва да бъде решен спешно На Линия Намерени Предупреждения Уеб Сървър Седмица Заповядайте в %s ден дни час часа минута минути месец месеца секунда секунди седмица седмици година години 