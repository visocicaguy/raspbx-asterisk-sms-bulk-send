��    7      �  I   �      �     �  	   �     �  
   �  	   �  O   �     0     F     J     V     [     `     l     ~     �     �     �     �     �     �  3   �                    &     /  
   <     G     O     n     u     �     �     �  8   �     �     �  
                  &     *     /     4     :     A     I     O     V     ]     e     j     p     u  �  {     B	     I	     N	     V	     _	  L   g	     �	     �	     �	     �	     �	     �	     �	      
     
     
     !
     %
     ,
     E
     L
     k
     r
     
     �
     �
     �
     �
     �
     �
     �
     �
          $  -   1     _     o     |     �  	   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     &                                    
            5   +       %          *   "       (             	   4             /                          -            )              $   7      .   6      #             '   1          ,      0   !          3          2    %s ago 0 seconds 1 Minute 15 Minutes 5 Minutes <strong>'%s'</strong><br><i>(You can change this name in Advanced Settings)</i> Critical Errors found Day Delete This Disk Hour Ignore This Invalid Selection Invalid period Load Averages Memory Month Network No critical issues found Overview Please check for errors in the notification section Resolve Security Issue Show All Show New Since Reload Statistics Summary SysInfo updated %s seconds ago System System Alerts System Dashboard System Last Rebooted System Overview This is a critical issue and should be resolved urgently Uptime Warnings Found Web Server Week Welcome to %s day days hour hours minute minutes month months second seconds week weeks year years Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-01-28 19:38-0300
PO-Revision-Date: 2015-11-16 11:49+0200
Last-Translator: james <zhulizhong@gmail.com>
Language-Team: Simplified Chinese <http://weblate.freepbx.org/projects/freepbx/dashboard/zh_CN/>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 2.2-dev
 %s 前 0秒 1分钟 15分钟 5分钟 <strong>'%s'</strong><br><i>(用户可以通过高级设置修改名称)</i> 发现严重错误 天 删除这个 硬盘 小时 忽略这个 无效选项 无效循环 平均负载 内存 月 网络 没有发现严重错误 总览 请在提示模板检查错误 解决 安全问题 显示所有信息 显示新信息 自从重新加载后 统计数据 汇总 %s 之前，sysinfo 已更新 系统 系统告警 系统状态 系统最近一次重启 系统总览 这是一个严重错误，应该马上解决 已运行时间 发现告警 Web服务器 周 欢迎 %s 天 天 小时 小时 分钟 分钟 月 月 秒 秒 星期 星期 年 年 