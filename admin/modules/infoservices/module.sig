-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1513045267.886
[hashes]
LICENSE = 8177f97513213526df2cf6184d8ff986c675afb514d4e68a404010521b880643
README.md = eafa18bbccc5ced69720ca8b15d662ddb6d549e9f32ea6137d08987eb5f79d12
functions.inc.php = c27d1af88807e83b37281c5623d265f23bebcda229b89fff9a4803f668416080
install.php = 879838a7600975ba811d27a7a53ef6195a1c731383c00c07377287062a4633eb
module.xml = cccec7c567fcac2d72f7f509988774986e8231e2dcfc6f6eff35baf7f45e8f77
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJaLz0TAAoJEIbOh3Rp0urZaYMP/0Zemc5H0KKz5KJePD7tCeoz
EHIJ5ommYGK9zP49nEyUPkZEBuYVNAiLzUmdVOtHiUD0Vx84aGdWYKDnqZYPje9S
0B6sOgFzjf8z2q+MvnWrnKvkuzrOuRsCJ3voBGVGh4cxIuo9FnF7PJ44Fs3DgK8z
FqflwvnDgMFdcSNg1N8N8k8+/sor2n4CT/zfIKM6gvxcoYm9qQQmm31PTjEJF1R3
viI1HpVndrLJEpPO8lqHrgPQxGl/Wi7LhKChZ0XIQvTibGBUWqBnFgPpgBunissC
5JvfR414dHuOEfj+4Njl3hha/yvGMBhX3+12R2VtSLK9NISaunrcfNqu+zn3/djB
pKXFPE6IaqaTP77RoOmWgGl6ds1IP81YaT/EcphyI22JIOBWkryXuPPGv27mu9aW
2bE7+20Az5SuEbwM4Xv0z5X4XvL2y68vmR5DYCrMsa+Szx1qtAdymi2Ca9opdu/e
bepLMbjuTBXmzAhthWs+uW6Awg8lujgOk3Kx2RObHealIVX7pjO5eXbbgzpQCkW4
4ZkO8TaUF8LBEkwwDwhKG51yWidMaEzWVrZkWs4Gj6iO1XXOjDvcTdBszVnpHuwA
oX2sAXi7ReE2gAzye8SvyeeraA3EGJAcquIb+oqySgIAkfiAU8YWQxAUkPAH/763
XQ5rw6GHTC/iY2HlD7Lu
=I37E
-----END PGP SIGNATURE-----
