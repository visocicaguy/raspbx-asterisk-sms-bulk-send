# This file is part of FreePBX.
#
# For licensing information, please see the file named LICENSE located in the module directory
#
# FreePBX language template for certman
# Copyright (C) 2008-2017 Sangoma Technologies, Inc.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-04-03 17:07-0400\n"
"PO-Revision-Date: 2017-08-08 16:34+0200\n"
"Last-Translator: PETER <ftek@ymail.com>\n"
"Language-Team: Vietnamese <http://weblate.freepbx.org/projects/freepbx/"
"certman/vi/>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4\n"

#: Certman.class.php:1252 Certman.class.php:1270
#, php-format
msgid "%s already exists!"
msgstr "%s  đã tồn tại!"

#: Certman.class.php:1764
#, php-format
msgid "(%s days)"
msgstr "(%s ngày)"

#: views/le.php:136
msgid "(Port 80)"
msgstr "( Cổng giao tiếp 80)"

#: Console/Certman.class.php:37
msgid "<error>"
msgstr "<lỗi>"

#: Console/Certman.class.php:40 Console/Certman.class.php:76
#: Console/Certman.class.php:83
msgid "<info>"
msgstr "<thông tin>"

#: views/ca.php:5
msgid ""
"A Certificate Authority is already present on this system. Deleting/"
"Generating/Uploading will invalidate all of your current certificates!"
msgstr ""
"Một nhà cung cấp chứng thực đã có mặt trong hệ thống này. Đang xóa/ Đang "
"khởi tạo/ Đang tải lên sẽ làm mất tất cả các chứng thực hiện có của bạn!"

#: views/overview.php:19
msgid ""
"A Self-Signed Certificate has been generated for you on install. You can use "
"this certificate now to get started however we strongly urge you to get a "
"real certificate from a standard authority or through Let's Encrypt"
msgstr ""
"Một chứng chỉ tự ký vừa được tạo cho bạn  lúc cài đặt. Ngay bây giờ bạn có "
"thể sử dụng chứng thực này để khởi động tuy nhiên chúng tôi rất mong bạn nên "
"có một chứng thực thực sự từ nhà cung cấp chứng thực tiêu chuẩn hoặc thông "
"qua Let's Encrypt"

#: views/le.php:121
msgid ""
"A certificate policy (CP) is a document which aims to state what are the "
"different actors of a public key infrastructure (PKI), their roles and their "
"duties"
msgstr ""
"Luật chứng thực ( CP) là một tài liệu với mục đích để nêu rõ vai trò và "
"nhiệm vụ của những đối tượng khác nhau của hạ tầng khóa công khai (Tiếng "
"anh: public key infrastructure, viết tắt PKI)"

#: views/certgrid.php:31
msgid "Action"
msgstr "Thao tác"

#: views/ss.php:21 views/up.php:8
msgid "Add New Certificate"
msgstr "Thêm chứng thực mới"

#: Certman.class.php:427 Certman.class.php:453
msgid "Added new certificate"
msgstr "Đã thêm chứng thực mới"

#: Certman.class.php:476
msgid "Added new certificate signing request"
msgstr "Đã thêm yêu cầu ký chứng thực mới"

#: views/overview.php:18
#, php-format
msgid ""
"Additionally if you have opened internet access up to the outside world you "
"can signup for a FREE certificate from the Let's Encrypt project. Learn more "
"%s"
msgstr ""
"Thêm vào đó, nếu bạn đã mở truy nhập internet đến thế giới bên ngoài bạn có "
"thể đăng ký một chứng thực miễn phí từ dự án Let's Encrypt. Tìm hiểu thêm %s"

#: certman.i18n.php:6
msgid "Admin"
msgstr "Quản trị viên"

#: views/up.php:206
msgid ""
"After you have submitted a CSR to a CA, they will sign it, after validation, "
"and return a Signed Certificate. That certificate should be pasted in the "
"box below. If you leave this box blank, the certificate will not be updated."
msgstr ""
"Sau khi bạn gửi đi một CSR ( Certificate singing request- Yêu cầu ký chứng "
"thực) tới một CA ( nhà cung cấp chứng thực), họ sẽ ký nó. Sau khi hợp lệ và "
"gửi lại một Chứng thực đã ký. Chứng chỉ đó phải được dán vào hộp sau. Nếu "
"bạn để hộp này trống, chứng thực sẽ không được cập nhật."

#: assets/js/certman.js:14
msgid "Are you sure you want to make this certificate the system default?"
msgstr "Bạn có chắc bạn muốn đặt chứng thực này là mặc định của hệ thống?"

#: Console/Certman.class.php:68
msgid "Base Name"
msgstr "Tên cơ sở"

#: views/up.php:168
msgid "CSR Reference"
msgstr "Tham chiếu CSR ( yêu cầu ký chứng chỉ)"

#: views/ca.php:89 views/certgrid.php:27 views/rnav.php:7 views/up.php:203
msgid "Certificate"
msgstr "Chứng thực"

#: Certman.class.php:1651 Certman.class.php:1666 Certman.class.php:1675
#, php-format
msgid "Certificate %s is not readable! Can not continue!"
msgstr "Chứng thực %s không thể đọc được! Không thể tiếp tục!"

#: views/ss.php:159
msgid "Certificate Authority"
msgstr "Nhà cung cấp chứng thực"

#: views/ss.php:171
msgid "Certificate Authority to Reference"
msgstr "Nhà cung cấp chứng thực để tham chiếu"

#: Certman.class.php:1759
msgid "Certificate Expired!"
msgstr "Chứng thực đã hết han!"

#: views/le.php:53
msgid "Certificate Host Name"
msgstr "Tên Host của chứng thực"

#: views/rnav.php:2
msgid "Certificate List"
msgstr "Danh sách chứng thực"

#: Console/Certman.class.php:21 certman.i18n.php:10 views/overview.php:7
msgid "Certificate Management"
msgstr "Quản lý chứng thực"

#: certman.i18n.php:4
msgid "Certificate Manager"
msgstr "Trình quản lý chứng thực"

#: certman.i18n.php:8
msgid ""
"Certificate Manager for Asterisk. Used for TLS, DTLS connection (think "
"WebRTC and secure traffic)"
msgstr ""
"Trình quản lý chứng thực cho Asterisk. Được sử dụng cho kết nối TLS, DTLS  "
"( think WebRTC and lưu lượng truy cập an toàn)"

#: views/overview.php:16
msgid ""
"Certificate Manager manages certificates for secure calling (TLS/SRTP), "
"secure web sessions (HTTPS/WEBRTC[WSS] and more"
msgstr ""
"Trình quản lý chứng thực quản lý các chứng thực để đảm bảo an toàn cuộc goi "
"( TLS/SRTP), đảm bảo an toàn cho các phiên truy cập we (HTTPS/WEBRTC[WSS] và "
"nhiều hơn thế"

#: views/le.php:113 views/ss.php:85 views/up.php:97
msgid "Certificate Policies"
msgstr "Các luật chứng thực"

#: views/view.php:10
msgid "Certificate Settings"
msgstr "Cài đặt chứng thực"

#: views/up.php:185
msgid ""
"Certificate Signing Request to reference. Select 'None' to upload your own "
"private key."
msgstr ""
"Yêu cầu ký chứng thực để tham chiếu. Chọn \"None\" để tải lên khóa cá nhân "
"của chính bạn."

#: Certman.class.php:1427
msgid "Certificate is empty"
msgstr "Chứng thực trống"

#: Certman.class.php:361 Certman.class.php:371
msgid "Certificate is invalid"
msgstr "Chứng thực không hợp lệ"

#: Certman.class.php:397 Certman.class.php:459
msgid "Certificate name is already in use"
msgstr "Tên chứng thực đang được sử dụng"

#: Certman.class.php:697
#, php-format
msgid ""
"Certificate named \"%s\" has expired. Please update this certificate in "
"Certificate Manager"
msgstr ""
"Chứng thực đã có tên \"%s\" vừa hết hạn. Vui lòng cập nhật chứng thực này "
"trong trình quản lý chứng thực"

#: Certman.class.php:723
#, php-format
msgid ""
"Certificate named \"%s\" is going to expire in less than a month. Please "
"update this certificate in Certificate Manager"
msgstr ""
"Thời hạn của chứng thực \"%s\" chỉ còn dưới một tháng. Vui lòng cập nhật "
"chứng thực này trong trình quản lý chứng thực"

#: Certman.class.php:727
#, php-format
msgid "Certificate named \"%s\" is valid"
msgstr "Chứng thực đã có tên \"%s\" hợp lệ"

#: views/ca.php:104
msgid "Certificate to use for this CA (must reference the Private Key)"
msgstr ""
"Chứng thực sử dụng cho nhà cung cấp này ( phải tham chiếu đến Khóa cá nhân)"

#: views/le.php:132
msgid "Challenge Over"
msgstr "Thử thách kết thúc"

#: views/csr.php:162
msgid ""
"City name such as \"Toronto\" or \"Brisbane.\" Do not abbreviate. For "
"example, enter \"Saint Louis\" not \"St. Louis\""
msgstr ""
"Tên thành phố ví dụ \"Toronto\" hay \"Brisbane.\". Không được viết tắt. Ví "
"dụ, nhập tên đầy đủ là \"Saint Louis\"  chứ không được viết là \"St. Louis\""

#: views/csr.php:150
msgid "City or Locality"
msgstr "Thành phố hoặc Địa phương"

#: views/le.php:100 views/up.php:74
msgid "Common Name"
msgstr "Tên chung"

#: views/csr.php:46
msgid "Common Name (Host Name)"
msgstr "Tên chung ( Tên host)"

#: views/csr.php:106 views/le.php:149
msgid "Country"
msgstr "Quốc gia"

#: views/ss.php:122
msgid "DNS name or your IP address"
msgstr "Tên DNS hoặc địa chỉ IP của bạn"

#: functions.inc/hook_core.php:120
msgid "DTLS Rekey Interval"
msgstr "Khoảng thời gian Rekey DTLS"

#: functions.inc/hook_core.php:106
msgid "DTLS Setup"
msgstr "Thiết lập DTLS"

#: functions.inc/hook_core.php:88
msgid "DTLS Verify"
msgstr "Xác minh DTLS"

#: Console/Certman.class.php:68 views/certgrid.php:30
msgid "Default"
msgstr "Mặc định"

#: Certman.class.php:212
msgid "Default Self-Signed certificate"
msgstr "Chứng thực tự ký mặc định"

#: Certman.class.php:616
msgid "Delete"
msgstr "Xóa"

#: views/certgrid.php:17
msgid "Delete CSR"
msgstr "Xóa CSR"

#: Certman.class.php:632
msgid "Delete Certificate"
msgstr "Xóa chứng thực"

#: views/certgrid.php:20
msgid "Delete Self-Signed CA"
msgstr "Xóa nhà cung cấp chứng chỉ tự ký"

#: Certman.class.php:505
msgid "Deleted Certificate"
msgstr "Chứng thực đã xóa"

#: assets/js/certman.js:5
msgid ""
"Deleting the certificate authority will invalidate all certificates "
"generated by this one (They will be deleted). Is that OK?"
msgstr ""
"Đang xóa nhà cung cấp chứng thực sẽ làm mất tất cả các chứng thực được tạo "
"bởi họ ( Chúng sẽ bị xóa). Điều này có được không?"

#: Console/Certman.class.php:68 views/certgrid.php:28 views/ss.php:135
#: views/up.php:122 views/view.php:47
msgid "Description"
msgstr "Mô tả"

#: Certman.class.php:203 Certman.class.php:214
msgid "Done!"
msgstr "Đã xong!"

#: views/certgrid.php:16
msgid "Download CSR"
msgstr "Tải xuống CSR"

#: views/ca.php:13
msgid "Edit Certificate Authority Settings"
msgstr "Hiệu chỉnh cài đặt nhà cung cấp chứng thực"

#: views/le.php:36
msgid "Edit Let's Encrypt Certificate"
msgstr "Hiệu chỉnh chứng thực Let's Encrypt"

#: functions.inc/hook_core.php:60
msgid "Enable DTLS"
msgstr "Bật DTLS"

#: functions.inc/hook_core.php:61
msgid "Enable or disable DTLS-SRTP support"
msgstr "Bật hoặc Tắt hỗ trợ DTLS-SRTP"

#: Certman.class.php:216
#, php-format
msgid "Failed! [%s]"
msgstr "Thất bại!  [%s]"

#: views/le.php:22
msgid "Firewall Validated"
msgstr "Tường lửa đã hợp lệ"

#: views/le.php:16 views/le.php:28
msgid "Firewall Warning"
msgstr "Tường lửa đang cảnh báo"

#: views/overview.php:17
msgid ""
"From this interface you can generate a Certificate Signing Request (CSR) "
"which you can then use to issue a certificate to use for this server"
msgstr ""
"Từ giao diện này bạn có thể khởi tạo một yêu cầu ký chứng thực ( CSR) cho "
"việc bạn hành một chứng thực sử dụng máy chủ này"

#: Certman.class.php:637 views/certgrid.php:14
msgid "Generate CSR"
msgstr "Khởi tạo CSR"

#: Certman.class.php:637 assets/js/certman.js:56
msgid "Generate Certificate"
msgstr "Khởi tạo chứng thực"

#: views/certgrid.php:8
msgid "Generate Let's Encrypt Certificate"
msgstr "Khởi tạo Chứng thực Let's Encrypt"

#: views/certgrid.php:10
msgid "Generate Self-Signed Certificate"
msgstr "Khởi tạo Chứng thực tự ký"

#: Certman.class.php:199
msgid "Generating default CA..."
msgstr "Đang hởi tạo nhà cung cấp chứng chỉ mặc định..."

#: Certman.class.php:209
msgid "Generating default certificate..."
msgstr "Đang khởi tạo chứng thực mặc định..."

#: views/overview.php:18
msgid "Here"
msgstr "Ở đây"

#: views/ss.php:40 views/ss.php:110
msgid "Host Name"
msgstr "Tên Host"

#: views/overview.php:32
msgid ""
"Hover over the 'Default' column and click to make a certificate the system "
"default"
msgstr ""
"Di chuột  đến cột 'Defualt\" ( Mặc định)  và kích vào đó đặt một chứng thực "
"là mặc định trong hệ thống"

#: views/ss.php:74 views/up.php:64
msgid "How long the certificate is valid until"
msgstr "Chứng thực có hiệu lực tới bao lâu"

#: views/ca.php:7
msgid "I Know what I am doing"
msgstr "Tôi biết tôi đang làm gì"

#: views/up.php:196
msgid "If you have a separate private key paste it here."
msgstr "Nếu bạn có một khóa cá nhân riêng dán nó vào đây."

#: views/certgrid.php:22
msgid "Import Locally"
msgstr "Nhập tại chỗ"

#: Certman.class.php:1447
msgid "Imported from file system"
msgstr "Đã nhập từ hệ thống tệp"

#: views/overview.php:23 views/overview.php:24 views/overview.php:25
msgid "Information"
msgstr "Thông tin"

#: functions.inc/hook_core.php:121
msgid ""
"Interval at which to renegotiate the TLS session and rekey the SRTP session. "
"If this is not set or the value provided is 0 rekeying will be disabled"
msgstr ""
"Khoảng thời gian để dàn xếp lại phiên TLS và thay đổi khóa của phiên SRTP. "
"Nếu điều này không được cài đặt hoặc giá trị được cung cấp là 0 việc thay "
"đổi khóa sẽ bị vô hiệu"

#: Certman.class.php:501
msgid "Invalid Certificate"
msgstr "Chứng thực không hợp lệ"

#: Certman.class.php:944
msgid "Key does not match certificate"
msgstr "Khóa không khớp với chứng thực"

#: Certman.class.php:959
msgid "Key does not match certificate after password removal"
msgstr "Khóa không khớp với chứng thực sau khi xóa mật khẩu"

#: Certman.class.php:1396
msgid "Key is empty"
msgstr "Khóa trống"

#: Certman.class.php:1406
msgid "Key is password protected or malformed"
msgstr "Khóa được bảo vệ bằng mật khẩu hoặc bị sai khóa"

#: views/le.php:39
#, php-format
msgid ""
"Let's Encrypt Certificates are <strong>automatically</strong> updated by %s "
"when required (Approximately every 2 months). Do not install your own "
"certificate updaters!"
msgstr ""
"Những chứng thực Let's Encrypt  là <strong>tự động</strong> cập nhật bằng %s "
"khi được yêu cầu ( xấp xỉ 2 tháng 1 lần). Không cài đặt các cập nhật chứng "
"thực của riêng bạn!"

#: views/overview.php:23
msgid "Let's Encrypt:"
msgstr "Chứng chỉ Let's Encrypt:"

#: views/le.php:140
msgid "LetsEncrypt only supports hostname validation via HTTP on port 80."
msgstr ""
"LetsEncrypt chỉ hỗ trợ  xác nhận hostname thông qua HTTP trên cổng giao tiếp "
"80 ( Port 80)."

#: views/le.php:8
#, php-format
msgid ""
"LetsEncrypt requires the following hosts to be permitted for inbound http "
"access:<br /> <tt>%s</tt>"
msgstr ""
"LetsEncrypt yêu cầu các host sau được cho phép đối với việc truy cập vào "
"http: <br /> <tt>%s</tt>"

#: views/overview.php:33
#, php-format
msgid ""
"Making a certificate the 'default' changes certificate settings in Advanced "
"Settings ONLY. It will force said certificate to be the default for options "
"in Advanced Settings that require certificates. It will also place a "
"standard set of the certificate and it's key into %s for use by other "
"applications"
msgstr ""
"Tạo chứng thực 'mặc định' sẽ thay đổi các cài đặt chứng thực trong mỗi Cài "
"đặt nâng cao. Điều này bặt buộc chứng thực được nói đến là mặc định cho các "
"tùy chọn trong Cài đặt nâng cao yêu cầu phải có chứng thực. Nó sẽ đặt một bộ "
"chứng thực tiêu chuẩn và đây là khóa để vào trong %s cho các ứng dụng khác "
"sử dụng"

#: views/ss.php:66
msgid "N/A"
msgstr "N/A ( Non Available- không khả dụng)"

#: views/csr.php:24 views/up.php:26 views/view.php:23
msgid "Name"
msgstr "Tên"

#: views/certgrid.php:5
msgid "New Certificate"
msgstr "Chứng thực mới"

#: views/ca.php:13
msgid "New Certificate Authority Settings"
msgstr "Cài đặt nhà cung cấp chứng thực mới"

#: views/csr.php:8
msgid "New Certificate Signing Request"
msgstr "Yêu cầu ký chứng thực mới"

#: views/le.php:36
msgid "New Let's Encrypt Certificate"
msgstr "Chứng thực Let's Encrypt mới"

#: Certman.class.php:910
msgid "No Certificate provided"
msgstr "Không có chứng thực nào được cấp"

#: Certman.class.php:196
msgid "No Certificates exist"
msgstr "Không có chứng thực nào tồn tại"

#: Console/Certman.class.php:76
#, php-format
msgid ""
"No Certificates to import. Try placing a certificate (<name>.crt) and its "
"key (<name>.crt) into %s"
msgstr ""
"Không có chứng thực nào để nhập. Thử đặt một chứng thực (<name>.crt) và khóa "
"của nó (<name>.crt) vào %s"

#: Certman.class.php:317
msgid "No Private key to reference."
msgstr "Không có khóa cá nhân nào để tham chiếu."

#: Certman.class.php:308 Certman.class.php:406 Certman.class.php:414
#: Certman.class.php:906
msgid "No Private key to reference. Try generating a CSR first."
msgstr "Không có khóa cá nhân nào để tham chiếu. Thử kích hoạt một CSR trước."

#: Certman.class.php:293
msgid "No certificates to import"
msgstr "Không có chứng thực nào để nhập"

#: views/up.php:173
msgid "None"
msgstr "Không có"

#: views/up.php:207 views/up.php:216
msgid "Not Shown for your security. Paste a new certificate here"
msgstr "Không hiển thị đối với bảo mật của bạn. Dán một chứng thực mới ở đây"

#: views/up.php:197
msgid "Not Shown for your security. Paste a new key here"
msgstr "Không hiển thị đối với bảo mật của bạn. Dán một khóa ở đây"

#: views/overview.php:33
msgid "Note:"
msgstr "Ghi chú:"

#: views/csr.php:62 views/ss.php:182
msgid "Organization Name"
msgstr "Tên tổ chức"

#: views/csr.php:74
#, php-format
msgid "Organization Name such as %s"
msgstr "Tên tổ chức ví dụ như %s"

#: views/csr.php:84
msgid "Organization Unit"
msgstr "Đơn vị tổ chức"

#: views/csr.php:96
msgid ""
"Organizational Unit. This can be a doing business as (DBA) name, or the name "
"of a department within the business. This may be left blank."
msgstr ""
"Đơn vị tổ chức. Điều này có thể là hoạt động kinh doanh dưới tên (DBA), hoặc "
"tên một phòng trong lĩnh vực kinh doanh. Trường này có thể để trống."

#: views/le.php:73
msgid "Owners Email"
msgstr "Email của người sở hữu"

#: views/le.php:30
msgid ""
"PBX System Firewall is not in use so this can not be verified. Please "
"manually verify inbound connectivity."
msgstr ""
"Tường lửa hệ thống PBX trạng thái không sử dụng vì thế nó không được xác "
"nhận. Vui lòng xác nhận bằng tay kết nối vào."

#: views/up.php:145
msgid "Passphrase"
msgstr "Cụm mật khẩu"

#: views/up.php:207 views/up.php:216
msgid "Paste new certificate here"
msgstr "Dán chứng thực mới ở đây"

#: views/up.php:197
msgid "Paste new key here"
msgstr "Dán khóa mới ở đây"

#: views/ca.php:62 views/up.php:193
msgid "Private Key"
msgstr "Khóa cá nhân"

#: views/ca.php:77
msgid "Private Key File to use for this CA"
msgstr "Tệp khóa cá nhân sử dụng cho nhà cấp chứng chỉ này"

#: Certman.class.php:621
msgid "Reset"
msgstr "Cài đặt lại"

#: views/ca.php:50
msgid "Select this for additional fields used to upload your own certificate."
msgstr ""
"Chọn mục này cho các trường bổ sung được sử dụng để tải lên các chứng thực "
"của riêng bạn."

#: views/certgrid.php:42
msgid "Self Signed"
msgstr "Tự ký"

#: views/overview.php:25
msgid "Self-Signed:"
msgstr "Tự ký:"

#: Certman.class.php:745
msgid "Some Certificates are expiring or have expired"
msgstr "Một số chứng thực đang hết hạn hoặc vừa hết hạn"

#: Certman.class.php:748
msgid ""
"Some SSL/TLS Certificates have been automatically updated. You may need to "
"ensure all services have the correctly update certificate by restarting PBX "
"services"
msgstr ""
"Các chứng thực SSL/TLS được cập nhật tự động. Bạn cần chắc chắn rằng tất cả "
"các dịch vụ có chứng thực cập nhật chính xác bằng cách khởi động lại các "
"dịch vụ PBX"

#: views/csr.php:140
msgid ""
"State or province such as \"Queensland\" or \"Wisconsin\" or \"Ontario.\" Do "
"not abbreviate. Enter the full name."
msgstr ""
"Bang hoặc tỉnh như \"Queensland\" hoặc \"Wisconsin\" hoặc \"Ontario.\" Không "
"được viết tắt. Nhập tên đầy đủ."

#: views/csr.php:128
msgid "State/Province"
msgstr "Bang/Tỉnh"

#: views/le.php:165
msgid "State/Province/Region"
msgstr "Bang/Tỉnh/Miền"

#: Certman.class.php:626
msgid "Submit"
msgstr "Gửi đi"

#: Certman.class.php:485
msgid "Successfully deleted the Certificate Authority"
msgstr "Đã xóa thành công nhà cung cấp chứng thực"

#: Certman.class.php:493
msgid "Successfully deleted the Certificate Signing Request"
msgstr "Đã xóa thành công Yêu cầu ký chứng thực"

#: Console/Certman.class.php:83
#, php-format
msgid "Successfully imported %s"
msgstr "Đã nhập thành công %s"

#: Certman.class.php:290
msgid "Successfully imported certificates"
msgstr "Các chứng thực đã nhập thành công"

#: Console/Certman.class.php:105
#, php-format
msgid "Successfully set %s as the default"
msgstr "Cài đặt thành công %s như mặc định"

#: Certman.class.php:683 Certman.class.php:710
#, php-format
msgid "Successfully updated certificate named \"%s\""
msgstr "Chứng thực được đặt tên đã cập nhật thành công \"%s\""

#: views/ss.php:97 views/up.php:109
msgid "TThe certificate policies"
msgstr "Các luật chứng thực"

#: Console/Certman.class.php:101
msgid "That is not a valid ID"
msgstr "Đó không phải là một ID hợp lệ"

#: functions.inc/hook_core.php:76
msgid "The Certificate to use from Certificate Manager"
msgstr "Chứng thực sử dụng từ trình quản lý chứng thực"

#: views/ss.php:147 views/up.php:134 views/view.php:59
msgid "The Description of this certificate. Used in the module only"
msgstr "Mô tả của chứng thực này. Chỉ được sử dụng trong mô đun này"

#: views/ss.php:194
msgid "The Organization Name"
msgstr "Tên tổ chức"

#: views/up.php:157
msgid ""
"The Passphrase of the Private Key. This will be used to decrypt the private "
"key and the certificate. They will be stored unpassworded on the system to "
"prevent service disruptions."
msgstr ""
"Cụm mật khẩu của khóa cá nhân. Điều này được sử dụng để giải mã khóa cá nhân "
"và chứng thực. Chúng sẽ được lưu trữ mà không cần mật khẩu  trên hệ thống để "
"ngăn chặn các gián đoạn dịch vụ."

#: views/csr.php:36 views/up.php:42 views/view.php:35
msgid ""
"The base name of the certificate, Can only contain alphanumeric characters"
msgstr "Tên cơ sở của chứng thực, chỉ có thể bao gồm các ký tự chữ và số"

#: views/ss.php:52 views/up.php:86
msgid "The certificate common name, usually the same as the host name"
msgstr "Tên thông thường của chứng thực, thường giống như tên host"

#: Console/Certman.class.php:96
msgid "The command provided is not valid"
msgstr "Lệnh đã cung cấp không hợp lệ"

#: views/overview.php:21
msgid "There are three different types of certificates this module can handle:"
msgstr "Có nhiều loại chứng thực mà mô đun này có thể xử lý:"

#: Console/Certman.class.php:86
#, php-format
msgid "There was an error importing %s. The error was: %s"
msgstr "Xảy ra lỗi khi nhập %s. Lỗi này là: %s"

#: Certman.class.php:326 Certman.class.php:421
#, php-format
msgid "There was an error importing the certificate: %s"
msgstr "Xảy ra lỗi khi nhập chứng thực: %s"

#: Certman.class.php:693 Certman.class.php:719
#, php-format
msgid "There was an error updating certificate \"%s\": %s"
msgstr "Xảy ra lỗi khi cập nhật chứng thực  \"%s\": %s"

#: Certman.class.php:349 Certman.class.php:389
#, php-format
msgid "There was an error updating the certificate: %s"
msgstr "Xảy ra lỗi khi cập nhật chứng thực: %s"

#: Certman.class.php:661
#, php-format
msgid "There were no files left for certificate \"%s\" so it was removed"
msgstr "Không còn tệp nào cho chứng thực \"%s\" vì vậy nó bị xóa"

#: views/le.php:24
msgid ""
"These entries are correctly set up in the Firewall module. However, it's "
"possible that other external firewalls may block access. If you are having "
"problems validating your certificate, this could be the issue."
msgstr ""
"Các mục nhập này được cài đặt chính xác trên mô đun tường lửa. Tuy nhiên, "
"các tường lửa nội bộ khác có thể chặn truy nhập. Nếu bạn đang gặp vấn đề "
"trong việc hợp lệ hóa các chứng thực, đây có thể là vấn đề."

#: views/le.php:18
msgid ""
"These hosts <strong>are not configured in the System Firewall</strong>. "
"LetsEncrypt will not be able to validate this host, and certificate "
"issueance will fail. To automatically add these hosts, please click on the "
"'Update Firewall' button."
msgstr ""
"Các host <strong> không được cấu hình trong tường lửa hệ thống </strong>. "
"LetsEncrypt  sẽ không thể xác nhận host này, và việc ban hành chứng thực sẽ "
"không thành công. Để tự động thêm các host này, vui lòng kích vào nút "
"'Update Firewall'."

#: views/le.php:81
msgid ""
"This email address is given to Let's Encrypt. It may be used by them if the "
"certificate is approaching expiration and it has not been renewed."
msgstr ""
"Địa chỉ email này được gửi tới Let's Encryp. Nó được sử dụng bởi Let's "
"Encrypt nếu chứng thực đang gần hết hạn và sẽ được làm mới."

#: views/csr.php:28 views/up.php:31
msgid "This field cannot be blank and must be alphanumeric"
msgstr "Trường này không thể để trống và phải là chữ và số"

#: views/le.php:65
msgid ""
"This must be the hostname you are requesting a certificate for. LetsEncrypt "
"will validate that the hostname resolves to this machine, and attempt to "
"connect to it."
msgstr ""
"Đây phải là tên host bạn đang yêu cầu một chứng thực cho nó.  LetsEncrypt sẽ "
"xác nhận rằng tên host liên quan tới máy này, và cố gắng để kết nối với nó."

#: views/overview.php:20
#, php-format
msgid ""
"To manually import certificate files place them into %s and make sure they "
"have the same basename, EG: %s"
msgstr ""
"Để nhập các tệp chứng thực tự động thì đặt chúng vào %s  và đảm bảo chúng có "
"cùng tên cơ sở, EG: %s"

#: views/up.php:212
msgid "Trusted Chain"
msgstr "Chuỗi tin tưởng"

#: views/csr.php:118
msgid "Two letter country code, such as \"US\", \"CA\", or \"AU\"."
msgstr "Mã quốc gia gồm 2 chữ cái, ví dụ  \"US\", \"CA\", or \"AU\"."

#: Console/Certman.class.php:68 views/certgrid.php:29
msgid "Type"
msgstr "Kiểu"

#: Certman.class.php:473
#, php-format
msgid "Unable to create CSR: %s"
msgstr "Không thể tạo CSR: %s"

#: Certman.class.php:442
msgid "Unable to find Certificate Authority"
msgstr "Không thể tìm thấy nhà cung cấp chứng thực"

#: Certman.class.php:435
msgid "Unable to generate Certificate Authority"
msgstr "Không thể khởi tạo nhà cung cấp chứng thực"

#: Certman.class.php:450
#, php-format
msgid "Unable to generate certificate: %s"
msgstr "Không thể khởi tạo chứng thực: %s"

#: Certman.class.php:940
msgid "Unable to read key. Is it password protected?"
msgstr "Không thể đọc khóa. Nó có được bảo vệ bằng mật khẩu không?"

#: Certman.class.php:1368
#, php-format
msgid "Unable to remove %s"
msgstr "Không thể xóa %s"

#: Certman.class.php:1476
msgid "Unable to remove ca.cfg"
msgstr "Không thể xóa  ca.cfg"

#: Certman.class.php:1471
msgid "Unable to remove ca.crt"
msgstr "Không thể xóa  ca.crt"

#: Certman.class.php:1466
msgid "Unable to remove ca.key"
msgstr "Không thể xóa ca.key"

#: Certman.class.php:487
msgid "Unable to remove the Certificate Authority"
msgstr "Không thể xóa nhà cung cấp chứng thực"

#: Certman.class.php:495
msgid "Unable to remove the Certificate Signing Request"
msgstr "Không thể xóa Yêu cầu ký chứng thực"

#: Certman.class.php:1481
msgid "Unable to remove tmp.cfg"
msgstr "Không thể xóa tmp.cfg"

#: Certman.class.php:923 Certman.class.php:927 Certman.class.php:931
#, php-format
msgid "Unable to write to %s"
msgstr "Không thể ghi tới %s"

#: Certman.class.php:1749
msgid "Unknown Expiration"
msgstr "Thời gian hết hạn không xác định"

#: Certman.class.php:631
msgid "Update Certificate"
msgstr "Cập nhật chứng thực"

#: views/ss.php:21 views/up.php:8
msgid "Update Existing Certificate"
msgstr "Cập nhật chứng thực hiện có"

#: views/le.php:19
msgid "Update Firewall"
msgstr "Cập nhật tường lửa"

#: Certman.class.php:748
msgid "Updated Certificates"
msgstr "Các chứng thực đã cập nhật"

#: Certman.class.php:335 Certman.class.php:358 Certman.class.php:368
#: Certman.class.php:392
msgid "Updated certificate"
msgstr "Chứng thực đã cập nhật"

#: views/ca.php:42
msgid "Upload CA"
msgstr "Tải lên nhà cung cấp chứng chỉ"

#: views/certgrid.php:9 assets/js/certman.js:52
msgid "Upload Certificate"
msgstr "Tải lên chứng thực"

#: views/certgrid.php:45
msgid "Uploaded"
msgstr "Đã tải lên"

#: views/overview.php:24
msgid "Uploaded:"
msgstr "Đã tải lên:"

#: functions.inc/hook_core.php:75
msgid "Use Certificate"
msgstr "Sử dụng chứng thực"

#: views/le.php:90 views/ss.php:62 views/up.php:54
msgid "Valid Until"
msgstr "Có hiệu lực đến"

#: functions.inc/hook_core.php:89
msgid ""
"Verify that provided peer certificate and fingerprint are valid\n"
"\t\t<ul>\n"
"\t\t\t<li>A value of 'yes' will perform both certificate and fingerprint "
"verification</li>\n"
"\t\t\t<li>A value of 'no' will perform no certificate or fingerprint "
"verification</li>\n"
"\t\t\t<li>A value of 'fingerprint' will perform ONLY fingerprint "
"verification</li>\n"
"\t\t\t<li>A value of 'certificate' will perform ONLY certficiate "
"verification</li>\n"
"\t\t\t</ul>"
msgstr ""
"Xác minh rằng các chứng thực được cung cấp và vân tay hợp lệ\n"
"<ul>\n"
"<li>Một giá trị của 'yes' sẽ thựa hiện cả việc xác minh dấn vân tay và chứng "
"thực</li>\n"
"<li>Một giá trị của  'no' sẽ khôngthực hiện việc xác minh vân ay hoặc chứng "
"thực</li>\n"
"<li>Một giá trị của 'fingerprint' sẽ chỉ thực hiện việc xác minh vân tay</"
"li>\n"
"<li>Một giá trị của 'certificate' sẽ chỉ thực hiện việc xác minh chứng thực</"
"li>\n"
"</ul>"

#: views/overview.php:12
msgid "What is Certificate Manager?"
msgstr "Trình quản lý chứng thực là gì?"

#: functions.inc/hook_core.php:107
msgid ""
"Whether we are willing to accept connections, connect to the other party, or "
"both.\n"
"\t\tThis value will be used in the outgoing SDP when offering and for "
"incoming SDP offers when the remote party sends actpass\n"
"\t\t<ul>\n"
"\t\t\t<li>active (we want to connect to the other party)</li>\n"
"\t\t\t<li>passive (we want to accept connections only)</li>\n"
"\t\t\t<li>actpass (we will do both)</li>\n"
"\t\t\t</ul>"
msgstr ""
"Cho dù chúng tôi sẵn sàng chấp nhận các kết nối, kết nối với bên kia, hoặc "
"cả hai.\n"
"Giá trị này sẽ được sử dụng trong SDP ((Giao thức Mô tả Phiên) khi gửi đi và "
"sử dụng cho các bên gửi SDP đến khi bên từ xa gửi actpass\n"
"<ul>\n"
"<li>hoạt động (chúng tôi muốn kết nối với bên kia)</li>\n"
"<li>thụ động (chúng tôi muốn chấp nhận chỉ các kết nối)</li>\n"
"<li>actpass (chúng tôi sẽ làm cả hai)</li>\n"
"</ul>"

#: views/up.php:215
msgid ""
"Your CA may also require a Trusted Chain to be installed. This will be "
"provided by the CA, and will consist of one, or multiple, certificate "
"files.   Paste the contents of all the Chain files, if any, into the box "
"below. This may be left blank, or updated at any time. They can be added in "
"any order."
msgstr ""
"Nhà cung cấp chứng thực của bạn cũng yêu cầu chuỗi tin tưởng được cài đăt. "
"Nhà cung cấp chứng thực sẽ cung cấp chuỗi này cho bạn và sẽ bao gồm một hoặc "
"nhiều các tệp chứng thực. Dán các nội dung của các tệp Chuỗi, nếu có vào các "
"cửa sổ sau. Có thể để trong hoặc cập nhật bất cứ khi nào. Có thể thêm chúng "
"khi có yêu cầu."
