-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1522793662.8464
[hashes]
Certman.class.php = 626f1c18056b2f9d84eb4da11888a7500cf183af561ac9c9cea80b9f51eb9927
Console/Certman.class.php = 59c0351131c662d54a688d7783d7915bdaac6de93d82c517d8141a0e06d98fe8
FirewallAPI.class.php = 36c9b82e6dda3e29e2cd0ccd656b57e3267fac65a111eb710af994cbb2a97f7e
LICENSE = 57c8ff33c9c0cfc3ef00e650a1cc910d7ee479a8bc509f6c9209a7c2a11399d6
README.md = 600d3ec6b9be38e5f1d94516416ce4c589c6c08ef643589e198b3d5e08543411
assets/js/certman.js = 4a66976357af6a27fc32a1141372401bb94885d255c22d8d36b15ab8099483d9
assets/js/views/regions.js = daeb80b449546558393506bb3f4ef1c835ce26ee156abcd11ebeeaa736de3454
assets/less/certman.less = 6c7422edb4fbed9b896e8f44b54e37f1c63dc163ee3d25687e3760133f425dcb
composer.json = ec8cbac35e6ca0dc0535a92c3b308d480eace16a175568a44feb81059b67d455
composer.lock = 820f8e3d88a4aa0a8431c70febd6d8dffb66676e2186f8af76248a0a3a6baa74
files/x3-root-ca.cert = 8abd468e10d5e4254d7cb2191a012df64a6aaa0671e810ac179d74a33c402f76
functions.inc/hook_core.php = d5d37e53fe56b1424cafdf96a6a03c1abc8788828107a607694ee5ce22072c56
functions.inc.php = d7b0d29b4ab3be70b674967d152ed137d0286a4613f4c1fd995d916a991fa09e
install.php = e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
module.xml = 08a14bd189fd8caa752301e9cc99020bc9ce27aa41bd40fb9601f3cdb931a1ee
page.certman.php = d385daa725bdad35d3e8d6fdf7772c582dbd2e78c444ef0dad36f8593283a9b0
uninstall.php = e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
vendor/analogic/lescript/LICENSE = 1046b522394079a88c0bb006ef97412216163692acde9410c30a1c5372f77c09
vendor/analogic/lescript/Lescript.php = abf1957fd1a25dde78230547887e751b66f4cc0bf0d0fe37716d0c08ac50be6d
vendor/analogic/lescript/README.md = 772ba16bc110f213b08066f6482c94ce507deaee06066401f9753628c9ae690d
vendor/analogic/lescript/_auto_example.php = 31d8cd4da4c9c0dfc4101f3cc6750ffd36430e02580b68cfcd6cd8d0f8607761
vendor/analogic/lescript/_example.php = c755ecd882488936a51b2e68a70b93ba5624d5f0c90b29b1f18b5a34c14f4fac
vendor/analogic/lescript/composer.json = 9a1eab15edcccd51810fe44613c85350eaae6f9f130e96ac0c17ddcd3747c0e3
vendor/autoload.php = 3932cac10667ba47c1f5d5dc14565df48892352254552bc5a82a3d5ca874cd46
vendor/composer/ClassLoader.php = 7ed0e500e81a43b8f106d0c19ace4e7352473ac7938e67424b8e5a69a4895f6e
vendor/composer/LICENSE = c8cce4b6b9729f264ffdf9296d505d63432497feeed1f586d1902b942197e024
vendor/composer/autoload_classmap.php = 894fbd23013969829316a4c244401c65355ee4d48440f2d533fb517c693b717d
vendor/composer/autoload_files.php = b33d844e584c1aee629ad31747bc97bfc55b20ca04818442a9d0b28bcdf66da2
vendor/composer/autoload_namespaces.php = 6d1b582b311799934568f00d45e01de7a04f8a1865a8a72c7841904ad4dac3a7
vendor/composer/autoload_psr4.php = 9f9e3a6d8eacfa740bb1b8c2b01a93495e9999f6f070a3cfa79e2f4fe5e30a06
vendor/composer/autoload_real.php = 3b935b075aefedadb2b60e68cdfb81e26af6691162f8e3d86fd442fe05e75289
vendor/composer/autoload_static.php = ccfa83b36978050acdccf3c9165f9620d61f2ec4d1f89f1523a85a44b48ba59b
vendor/composer/ca-bundle/LICENSE = 37b21ffcf3e606669896eb33ec7559a745df1e70cbf0372a6f56d977cf6f0c37
vendor/composer/ca-bundle/README.md = f6f30798e52393a8e290821aa1f5b02d1390e33a839b666a3267592783b2aee4
vendor/composer/ca-bundle/composer.json = 0a0cb4abc482d739fe2043ce0bd91cdc2baa177c87514acbee438285e5738445
vendor/composer/ca-bundle/res/cacert.pem = 79ea479e9f329de7075c40154c591b51eb056d458bc4dff76d9a4b9c6c4f6d0b
vendor/composer/ca-bundle/src/CaBundle.php = 3dca567a1831bf258782bb7e59d9280fd24bacd8efb6dbd7121f45ba78d2ee7d
vendor/composer/installed.json = 28cdd6e5e230922fe7639a0fa70a079c9b0f23dbe0ae3490a2508ee852044641
views/ca.php = 7ca0e3370071bfc6d52f470c0a199f636f778ee0ae962f51233994eccb6c8e68
views/certgrid.php = 3d896a314c67dbf20740bfe9c99a6cbf1de6fc0a63d6aaa58a84396ed8e7d2cb
views/csr.php = 6e976472b26305a3306e69a91eb2f3fd82d915197538a5fbb22ea1ab9382f0f4
views/le.php = 87e2f587d128bf3166d34d5bb7a18434a7fc54669713f32b1f76537c01c1b8f0
views/overview.php = 0ef522541f82c77fe400937cc2981b5f870ee536afeb94615d1b240220992ae3
views/rnav.php = f6438da02bd269b3b5fe06593585786e900848341ed279c34c3b5d99dfb9f661
views/ss.php = 0f229962e1d5c89c42462166e824c9a772ae0dbf93a5b49c849a1fc717244bd6
views/up.php = 4c1e2008669d9d3489dc1434cb123585bfb97be85d307e953a168934207b6f2d
views/view.php = b2970bde5e153f204464a17715ac47d53ee755c393ac730a9ba12e03a16a7dd6
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.22 (GNU/Linux)

iQIcBAEBAgAGBQJaw/y+AAoJEIbOh3Rp0urZL+oP/3xdyathcSrmkkZqcKvDvhtY
Rnign5zog/Bt2wOCtLn5YzZEeCH6RYuYCyaQiNoadA6w/vtQrLyWbI/IiDTBI4sU
3qq35CrBjAh+UFJDc7aIv8rTQwG77rvzvdRCq1irR/41Zq3JrtGkKS5IfAGJxd2A
yoKfAH8nhVIDNDo8T+DYXA9KHNnpcs85P2zqiGpfVR4u5HQio88oDScRDktx0Rc4
XJ6RvtEvREEtm8ySawqX05cxLDwQHtSuMsW5olRt1GVCtpUXB48+urKUn3k6+uon
ef1kK+zEv0+1GcnrMOf8IdEd1fFooTuaApaAV7spwxe3hwy24yMCPzVuDowQ87r/
Aq1fpkxhUwVt+KsqyNDS2k7VkgPSUy2QQPZb7bKHbfEjbGkbOKg5un5NlDANU+nx
0gyikPHW7oCF05Y2fi8eovpW/iBeDiBl1wSUf30WGDif0y3sflObuQ+YQWlXflGo
aFCWQ65JcCIQ8IDUCDI0EYiSjVEu6xsj1V5/WfR0YOwThinFW3pXmtwrIbQsqdad
kg96GPLNfx7wsjLd8UhRYk+auJ49WN6zOAwV5ztQ2x/5j4Si9FOF2OOERtAhBx8o
C3+3U+cdllOikM2750t3QY4yb5+5/h5DdIE6qzuas/4rPEHRthP3u682BuUEoBxt
lBzG7H2A9rqufdyZxXXB
=1pwK
-----END PGP SIGNATURE-----
