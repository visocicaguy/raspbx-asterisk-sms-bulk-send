-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1500413020.6333
[hashes]
Asteriskinfo.class.php = 71271b05f7122188529cf7debdea9204d59c0cbf439e5f0af655d5af81fc6668
LICENSE = 8ceb4b9ee5adedde47b31e975c1d90c73ad27b6b165a1dcd80c7c545eb65b903
README = e3e49cc6d19b0d37c4b37d0d688ad56e91181a695164abb73a0e3d12f78f5e4d
README.md = 8a064892f47d67cc7029fb5426922530c128b0087bb6fd857e3797fc1a87cb4a
asteriskinfo.css = 9d5f236c097ed831d1fbae1427d06641288e7b6dac2d425e37692ff071c8e720
functions.inc.php = 49a2670f6ff8cb08efe9c40517c7fc98d9986e33271147cce83cc0a984500c10
module.xml = 779ce9bae513dddc07db84a2390f2ba8992775bd7693432b10122147500b2fff
page.asteriskinfo.php = ed94b2275deffe9e8b9e58e5ccf293149050d74ae235ec6b6cc6c24fbd7e4985
views/bootnav.php = 2cbaae628546833e66bf13a5018f63901795facdd51f18ac01f18d579543a42c
views/panel.php = a0c5618c06ac0ad5eaab12416a6e2c2ad0d1832d55ac6243ef62fc5462fa494b
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJZbnxcAAoJEIbOh3Rp0urZCfUP/jP/ZbXCKPTWXiVe9svl31B8
+4RHgAY6dWiFMTsbMFeVgdI0HZhfsUi0WWQL9SCgL5y8G6nl750n1HL8/V7YXiyz
DJt9+KhG4mCJJXCiOBcOlrzU2C85WY+RqlKlAY7VTb6CM+3QWYbpCxA9uQiN2CVY
wJqnbkL3L/ZVellJuVBMIwIyloC1YquF1njsw6tOEFAVXsQJZVClgsBYrA4hK+g0
PYHV/YNh8zUJC+hMTw19SaM/x3OlOZkuSZ0PtfJpmWXh4neaV9g2Zzw1YOlh3Uom
R84uY+mLM2HMbQGPPRBhiQ/uK0DhD6Snhp9J6Gd1o3vCMj+m42z/5fglQAT5hldj
U9WWh30HzwF7OrfuS8hLUOCyeTti6835RrUKt63gda6f57bJW4vXtPAdHiRm7W1Q
JUzFspucZv6ggObVrpLp/bhT/O9hUmXP6QkaYQ3bmsNMDB0WDEh4s0aQqFp6/ci/
XoRzwAtJUNuzOtv7QrBBAk0B5mnUbJnro3scYYhAiSlNYxQkwez5NJHC9TrqriPH
qcRo+6kHYG/uj7jc3ZfX+uUcn+mUaRySyD31U6RJwYldHauIs0NHAy6brsbfC2Vl
vEsEpJTxK3SP3xRh19ysceYOw4oWtaDvx6uwyYM/Zz5T9dnFnIgBK3QoIKbB/Eny
wt5/To2jlWvomlYTNNYd
=gxxz
-----END PGP SIGNATURE-----
