��          �   %   �      `     a     s     �     �     �     �     �     �     �     �     �  	          9   "     \     c     k     z     �  5   �     �  	   �     �     �     �     �           %   +     Q     d     s  %   �     �     �     �     �               0  c   G  
   �  
   �     �     �     �  k   �     j     �  "   �     �     �  4   �              	                                 
                                                                             Active Channel(s) Active SIP Channel(s):  Available:  Channels Conferences Current Asterisk Version: DAHDi Channels Full Report Google Talk Channels IAX Info IAX2 Channel(s) Offline:  Online:  Provides a snapshot of the current Asterisk configuration Queues Reports Sip Registry:  Subscriptions Summary This page supplies various information about Asterisk Unavailable:  Unknown:  Unmonitored:  Uptime Uptime:  Voicemail Users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-18 17:20-0400
PO-Revision-Date: 2016-12-12 18:42+0200
Last-Translator: Maxim <maxim@ua.fm>
Language-Team: Ukrainian <http://weblate.freepbx.org/projects/freepbx/asteriskinfo/uk_UA/>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk_UA
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 2.4
 Активни канали Активних SIP каналів:  Доступно:  Каналів Конференції Поточна версія Asterisk: DAHDi канали Повний звіт Google Talk канали IAX інформація IAX2 канали Вимкнені:  Підключені:  Забезпечує знімок поточної конфігурації сервера Asterisk Черги Звіти SIP реєстрацій:  Підписки Зведення Ця сторінка надає різноманітну інформацію про сервер Asterisk Недоступний:  Невідомий:  Неконтрольований:  Час роботи Час роботи:  Користувачі голосової пошти 