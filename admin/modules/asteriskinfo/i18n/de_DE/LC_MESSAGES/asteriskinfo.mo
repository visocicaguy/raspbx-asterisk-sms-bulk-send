��    3      �  G   L      h     i     {     �     �     �     �     �     �               4     H     V     e     w     �     �     �     �     �     �     �     �     �               &     8  	   N     X     m     v     �  9   �     �     �  
   �     �     �               ,  �   4  5   �     �  	                  %     .  �  >     �	     
     #
     :
     O
     f
     s
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     	     $     2     8     O     d     t     �     �     �     �     �  	   �     �               *  @   B     �     �     �     �     �     �     �        �     =   �     �               1     >     M                 )              !            3   +                   &   	         2              /   %                  *       1         ,          (              #      '             0   "          -            .                $              
           Active Channel(s) Active IAX2 Channel(s):  Active PJSIP Channel(s):  Active SIP Channel(s):  Asterisk Info Available:  Chan_PJSip Channel(s) Chan_PJSip Endpoints Chan_PJSip Info Chan_PJSip Registrations Chan_Sip Channel(s) Chan_Sip Info Chan_Sip Peers Chan_Sip Registry Channels Conferences Current Asterisk Version: DAHDi Channels Dahdi Full Report Google Talk Channels IAX Info IAX2 Channel(s) IAX2 Registry IAX2 Registry:  Jabber Connections Motif Connections Offline-Unmonitored:  Offline:  Online-Unmonitored:  Online:  PJSip Endpoints:  PJSip Registrations:  Provides a snapshot of the current Asterisk configuration Queues Queues Info Registries Reports Sip Registry:  Subscribe/Notify Subscriptions Summary The module was unable to connect to the Asterisk manager.<br>Make sure Asterisk is running and your manager.conf settings are proper.<br><br> This page supplies various information about Asterisk Unavailable:  Unknown:  Unmonitored:  Uptime Uptime:  Voicemail Users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-18 17:20-0400
PO-Revision-Date: 2017-03-06 01:57+0200
Last-Translator: hammer125 <auswertung@aon.at>
Language-Team: German <http://weblate.freepbx.org/projects/freepbx/asteriskinfo/de_DE/>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de_DE
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.4
 Aktive Kanäle Aktive IAX2 Kanäle:  Aktive PJSIP Kanäle:  Aktive SIP Kanäle:  Asterisk Informationen Verfügbar:  PJSIP Kanäle PJSIP Endpoints PJSIP Informationen PJSIP Registrierungen SIP Kanäle SIP Informationen SIP Peers SIP Registrierung Kanäle Konferenzen Aktuelle Asterisk Version: DAHDi Kanäle Dahdi Vollständiger Bericht Google Sprachkanäle IAX Information IAX2 Kanäle IAX2 Registrierung IAX2 Registrierung:  Jabber Verbindungen Motiv-Verbindungen Offline-Nicht überwacht:  Offline:  Online-Nicht überwacht:  Online:  PJSIP Endpoints:  PJSIP Registrierungen:  Liefert eine Momentaufnahme der aktuellen Asterisk-Konfiguration Warteschlangen Warteschlangeninformation Registrierungen Berichte SIP Registrierung:  Abonnieren/Benachrichtigen Abonnements Zusammenfassung Das Modul konnte keine Verbindung zum Asterisk-Manager herstellen. <br> Stellen Sie sicher, dass Asterisk ausgeführt wird und Ihre manager.conf Einstellungen korrekt sind.<br> Diese Seite liefert verschiedene Informationen über Asterisk Nicht Verfügbar:  Unbekannt:  Nicht überwacht:  Betriebszeit Betriebszeit:  Benutzernachrichten 