-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=unsupported
timestamp=1513708713.7903
[hashes]
LICENSE = 8ceb4b9ee5adedde47b31e975c1d90c73ad27b6b165a1dcd80c7c545eb65b903
Motif.class.php = cf759865dc93ac01df26f1df532baa0d1cd42c4996c89f2ea5dc5913b0425efd
README.md = f55e329c0b2d3e7f0268acd934e854c2a472b3d77d7ea725c971908965e05f87
assets/js/motif.js = 6d8a82375656dc3ce24b85249876e41928d507bec627de5b3b96ff8640fe703e
assets/less/motif.less = e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
functions.inc.php = bf589102bf37abcb00a1009b031eae1865cd1d67c40e77724fb2bdfac9818af6
install.php = e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
module.xml = 5bd4dec1b3aa195f89b9d9bad70e9e2029f3db36039364a196d662edf079e47e
page.motif.php = 01554f5d6e12b992d77a8aa7396bc2de8767d815b4d1d27b7798b027dad66b4c
uninstall.php = e5284fb512304682bb678ec6aa2afd10bed0b338a32b9f35a27daf40186a4a92
views/account.php = 9ebc61b4d7e659797f052a5a4cb0c4e50a2a6ff3d616d7942c5d76c0866daa31
views/grid.php = 5ad1c5b7e185c17312d5514e441ad5369a8e82ca599480d0cf5a29d744729947
views/rnav.php = 86dfae5e7446607ce071876dd84a62f9dd155804d07e0bc46b4310f35a51ca59
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJaOVypAAoJEIbOh3Rp0urZu7kP/jX08FD6/L05vsimkgKZCMIm
yVxTqk/I3H0/5ZtSc8tMn4lphEyRznhGKFB9GN/3JEnadY40co1BEIteKTQBVd0I
oOYI5PsP+I6fiO/xlAfB+IBE3bjhpvfY+DnKwmbhEtNw2mqqaucLTp5h9yCfvKX6
icvagral0nPiNUUgCPMT0biNeQfBY4JGMyf46cEfVqi2P2PXYZcjwfHBiDw+OXDt
4E63bTLtbsz7A9BqxIVC7MhuowGNMXKFtbsBAYuvPMNeYc88TKucFVImfKCq+lFz
eXhondDQpXrD4oqXWH3piVY7kbeYDsk8uvCwfLL0GV8NmY5uMqZCk4DPIHde30Y6
G3UU0q0/jCJu70VR94N6PLRYV4R85ySasKuRX+1IQ+0IYZu+o41J2XAaPsTZrmS+
Hp3GOuvkwREJZaEYm/69I8IfEu4x+W5HCwuh3A/TQK+lY/gPMMhhRO6/QSOoQoxw
Ob6Uoq6Qk2Vcbz0VsNrSZhvFWGC/3RETCGvVcfX3NTzRAOsbVJwQSf62kfQkYc4t
7zoMzKudULgSUbVm95zwZT24uaK90bvBa9hiP6mkMRxNSlvWdV0vRPNeC5CpA38B
5LhfGSHwOK2Wm0Ud9XBB7d+imgwIuk3U22NHUCVrTh3b5A1Kry8LSQe5RhjEAA5m
kymxMEiYsB67iYAXo/OH
=wcBq
-----END PGP SIGNATURE-----
