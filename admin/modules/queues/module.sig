-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1521060721.7233
[hashes]
LICENSE = 8177f97513213526df2cf6184d8ff986c675afb514d4e68a404010521b880643
Queues.class.php = 75d0ac0b542220619c19df55840445eb56d4d2adfcb2711764d02fc3c917eaa3
README.md = b263cbb671adedaebe27d9b59f34d913031f467792fcdc3eb997ef66f2700e56
agi-bin/queue_devstate.agi = b2e0cbe8fe6bfa40985fec081b3b8342a0e92e14c2366805abfbbb9b4d01bc99
assets/css/jquery-cron.css = 482f60d1a67b51527cb7b2e07d2c092b30b16a61807aa90483ae2af7667e603e
assets/css/queues.css = 13f398973185dd78c5a2bb13701cd13549cfe48cba53add51876f22e98db8cca
assets/js/jquery-cron.js = baee4d4dc2669c84fc7379dafee568504839e7d3e67fd50af0446bbca61366c2
assets/js/queues.js = d5af0bdd619a52b856d89ed682059576d2b75d9ee8e6a0f054a83f66e003abd1
bin/generate_queue_hints.php = ef07b350721c7e1e0c057792a729eebb25426f9983b51d14c604043e10487f20
bin/queue_reset_stats.php = 07597ec29f9d2edb388f04ee9c6aeabb015bcb25c35e2a27914605e256677ff7
functions.inc/cron.php = be00d333bf749ac15d247af3109ed098872b8bd92e081109ed523cfce5378920
functions.inc/destination_registry.php = 40e0bc6104125b0436ef6b4bd9a91bc210b48e68a8a4e1027d09fba023eb39a7
functions.inc/dialplan.php = bdbe5acb2a9e0f94eb0d83eed3b3d545f2a73e18b90d97902103b769f609e235
functions.inc/geters_seters.php = fa35579b4544ed55a9994171f3e2cd9b1692acc34ce49b3b8403b5e0d24578f2
functions.inc/hook_core.php = ed1104e27ed8813bb3ebdd9687bba7701a5fa58f2e40afc3facd942044372d0b
functions.inc/hook_ivr.php = 99a4c9480ef1d69713806d7674ffe4ee36fa7648dc7fa2cdb6c523307ac9a20a
functions.inc/queue_conf.php = 48478f62f4e0529be8b573bacf4e4a255d338f1064d4ced3273276997602e225
functions.inc.php = 01adbead06c47bb0ec9c227e0a7e3c24d3f5fdf82eade7ac61d42de8335088aa
install.php = 999b39e99acb58e680599e89190993880bde406ed35e960d2a291d9ecd3fd857
module.xml = d923c248431b5c2efb579d7daa75bbdfc5ebcc66e97bb6d86196e180d74fcf7c
operations/Devstate.php = ebb6f1714bb7a2d126cfbddfa50796146051593aa483e2f3f20b047996c2a432
page.queues.php = 53dbf049999cf77202965d67d13c385688e31fe1d966ad3f89f942285bea04dc
phpunit.xml = e3ced4265e1f6ef7b8e88b23277c585161fc89fea71207e5fcd2e7237268cb2b
uninstall.php = 3653accdfa9c77740e5a67f47a8ce3cc90d3629382f02dd0bc400562ab3b2280
utests/DevstateTest.php = 7b18f23489a59792aea013757f4e061ee55f611793bae1ff9ad3ae04bbf0f2de
views/bootnav.php = 43674f4bb1018370546b0a6404dd6092abe0cee0d854aa42476ab479c037abe2
views/cron.php = c61ac8db93f5e1485aaa8b03e35032e341ffabbefa41d82566a76beae8b7b1a1
views/form.php = 910fba21ef973073de82864947724b1a02dfbd09964b2089e3260e90b36b016c
views/qgrid.php = 0a14afb7eea1debd8f87d57b940311cf893b15d52b08745091195c7c6f54e4a1
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.22 (GNU/Linux)

iQIcBAEBAgAGBQJaqYtxAAoJEIbOh3Rp0urZ4ZUP/2KGlswWmBcIzUabgnuR31fg
1W4LP2Mf5pUxOpfkhoZ1vt8w+XTW42hSILW6iH9siprp0YZzbKIn9msvcAW/jDIC
R5Yv+fM52qylRAPGQyteTqHoVvyoeu3JojuxjAx7YeWDK6JepfuMObUC9sxMd9Jm
1hJ1Q0JT4JxpuQ39aiql4MZktr5tC4t62Y1Eu8Jfjh9WUfq4cFWWwJ8pgxEzS5pY
rK2iFy40qJJAM3QVjtuExq67l/jR+DxneURHssiMi0gIdUcPJVVwO4GjoBnavS8u
zNRaKL8YQKUkcmHsjw1jZ+Kjbn+g/tKVQUjsKN0a0z/2GLVmEVLibXNMH7fFw1tZ
9+4dWeH2FfjEa/JRWQzDW8mGdjzu+2Mcm7KaHDR0IEl+2egye7saYB09zohSsY3r
U/1a5gpp2niTTxABBOhmMu0GDNRIH7KYh4l1iVpzOneG3QS9Y6Igy7jOkvJCnZFL
PXMJDJp5SNqg9e83mA99yzpgEBAFQFDuG5XyYyKCtNYStRvjSeHo6kwB75iOGoQm
SafuycOwzLd9oA5QwAjUHRVGvl96KWVI+XloBcQzQ1rW96x832m3/4JIKy77L71e
q3exJ92aw47IRo01GJjo6lflRhSW+RGs4flGZyjEjBjdD8Hzrue9OyFbNStoaWJQ
viZ3pO4LDrHY4Ce940Fv
=mdiR
-----END PGP SIGNATURE-----
