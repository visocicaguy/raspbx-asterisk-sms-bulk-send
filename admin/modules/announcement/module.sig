-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1498227170.1137
[hashes]
Announcement.class.php = 0eabc8340b551fe0b30ee3f8b3cdb7771a880964f07907f5607c021b11a37995
LICENSE = 8ceb4b9ee5adedde47b31e975c1d90c73ad27b6b165a1dcd80c7c545eb65b903
README = ff97f38a2989e0c476037fcd47ecc70facde86823d13f65abf9384fcffce381d
README.md = 616971fbd27233e6a2b3d0bfc39b00fc06a667825f41c6121ce6f3c307edbad3
assets/js/announcement.js = 94739948487d083bad4c2fd3676d67fe6c1704e5f5465fd1700564c97462309a
functions.inc.php = 868b8ae73517835adfb6da9d2f91c83b3fa470ea3b1392fd26357e2e01cb0c46
install.php = aa420da1ce32370ad6c93221f7b7e2fc131c940e97a39679c672b43460a3c5f8
module.xml = 51c1a93a2604dfd2be4d074a168d74f855edb5f95f1e05ae20645db4f36e3c18
page.announcement.php = ead08b195fec0558be63bb225b671c873274f8b280d98b860fc9f43c89fa7be2
uninstall.php = 36f59c1379d68c8210df19a438cc8d549a3530a1957c8ee6f61dc7963f1e77d6
views/form.php = 5e983c8fad6160f65bd909c559310a772ec08f01620aab858fb9bf00d3e4519a
views/grid.php = 63f9b7395b336bd1debdd77ffc95275018a79a45a95360c3da8f10a30eccb58c
views/rnav.php = c8ecb31d0c4a0c78ea3658474a40ac4d82d8e647581fa0faa87e8bb46aa8c20b
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJZTSHiAAoJEIbOh3Rp0urZMFwQAIG+K5xbbBTTuGA0DhioHNvy
RQ/OHeX2faNnu1G5tr/Em5fVKd7/kxqgHFmQHS7IBFKaYWQ7HxaBadcYKbvyMbEO
4nUUBLUADkaJybrxo3+InLcXQvprxUeZg+Qb78ZOFWb2bKN96yyzPjZ2jUjNgsFL
t4Wm87xhjX/lKlJXAU1T8YxAgrAkltgKunn92oz8NS2VKgKkJe0HQT8Xum7a64WX
LPXblIJD68YnGGHMuD44S78Vb0rAl6sFM97vkPFAejPGBp6o1aaoTMkLvZDCliLr
9TerXW0Tk5vujSp9HSrMvYjYnVhqyKGLXQmgGTMVG0R8CIXhwF/UR9dLkJW9pz8J
UF5+YaXqmElkwvu9ojCEzeq4ZfHMkOpEOSGxfIjcVIC1Yqk5fex+YmyNyXBIHswO
dKNQtZNcva6BP/PNuM+tsWe2cClE150l9FKGKhfCRaork+wGRRP7CE82Q/CmwRoW
EjNaHOKigqBuq0ndNESRcg+iIIWpHfjdFKr/r6sH9BYgSfxIIOkjo/cRWycGuawj
wXwpU18Dvz7Em+ajmX+XhDpUGVjGyFznzgioelE4Rm5F130+jFj8NRhoJSEw7RIv
PLvOgOQrVMNYuY6eYy6u7JCUh3gh7NXt8r/NhGgNqFqbk1MiISu5jU01ShlFwvb/
WjbSaD8wwzmTnn6T9X9S
=RK91
-----END PGP SIGNATURE-----
