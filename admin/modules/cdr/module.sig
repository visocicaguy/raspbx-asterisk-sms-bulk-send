-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1519804253.8846
[hashes]
Cdr.class.php = 4645578d0357c70567e523860b34301e77e663319010cea9ff0cc94ef69892ad
LICENSE = 8ceb4b9ee5adedde47b31e975c1d90c73ad27b6b165a1dcd80c7c545eb65b903
README.md = a56cada54cf8ed485b4f21851e6ce2ebed888dfad6f8814cf99945763385d021
assets/images/cdr_download.png = 84791c2e194dbd723563b2238a68f799ce216a4bbb6c800c3028af8ff222ef84
assets/images/cdr_sound.png = 27d79720cd3ce32cad17bf7f9b19799f9a5c9c77bec01545e6f3524d05cdad50
assets/js/cdr.js = 98ab40ec62deaa83f78682e052744b30f0cc596bf19b1b6dc05d4977284894a0
assets/less/cdr.less = e1fcfd1827b09227ffe13b434cbbe251d2700be0c5f44e27083ddbe1449f6728
cdr_play.php = 91e9cfea232a596df3a8d32a27c23163d0b57f08d97d69f8634e8605ce4709ad
crypt.php = 551fecda0fb3b89c7db38e008c4b44f96a9ffd65645090cc26dc6a0c9d610bd0
functions.inc.php = 50d67dca45fabdcfc03734d086f47cc461a679bc57176961e566f0444bd899ac
install.php = 6f4d0fff87275dd705c21eb8aaddeadd1afc55280af02697c6787dd6fb0a6a57
module.xml = f78429f9e704d06be7068a0edd952c2df5776a354b62b990369616e04579d9f3
page.cdr.php = bc5a2c4124771afa52f9cdd5548d0989433e4163994115bfb7fd4b812fb509b0
ucp/Cdr.class.php = 283f10707cc45a20a4bf5f8102a989f6387be20aa80854d3632eb45bb0a1a480
ucp/assets/images/jplayer.blue.monday.jpg = ab94479128a0f02fd2facb680a307e00a8b8bf2a94adb0399b1fd984e49528b5
ucp/assets/images/jplayer.blue.monday.seeking.gif = 00d752a8e28100f2183e5e837b99c35840b5b9e5a35a62d3551670258f58f705
ucp/assets/js/global.js = 8a815dfe0b914bcccf6033568b708580dfd6189c1967305e25b488a800caeb5b
ucp/assets/less/bootstrap.less = 26cc38836aba91e45785338a9f674ced8430dec144b40b0e082e4d004be8d905
ucp/views/widget.php = 763d9217828053e313119979ee62b85b5f8a121430144c97bc8e00ac97563a23
utests/DBHandleTest.php = 1b9a95262c7ce1f29509918a5f56046f70006d6466264ff84ffe8a3c16c7f498
utests/setuptests.php = a3d001240510806690e836f5a809133f8fc84530b8e7ff1474b8d5cb057fd4fa
views/ucp_config.php = 98b5bbfd42118607157c42346b8fd22fcef004c8a8ee8438a7ee3121d37cacf8
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.22 (GNU/Linux)

iQIcBAEBAgAGBQJall9dAAoJEIbOh3Rp0urZBsQP/iUOLraWewe4fmRZAgxFm5pS
FviGGCPLv0v++T+p54azgF4BpMxFHlvDiQMyWJSd7iI0fcXoaenB+Z7NbzoCWJ+w
GsQoYgEdVsjZg7fe61aSL3CbRMBZ1J+Ri5Yr13samkrsMqCnl1+Q3FpZgqQcfJOG
4ZPwTzcmqgK0WmTwWS5bpBeL0RWB/Co5oq37o17ocfa7qrF8sppmpmO93dMwqbHE
4RA7C+XwEeHnPFbI+oO1KiZUYm8nUgL0iT/g+ZETGGv0vu/jUAyynxre0AhFHE5j
LPKdcOhjGO2aE4SSUaXYWOW+AXjMgqpT94eRo8AKhTMuaXA/VyV/A7D91U1az5HV
Fa6lymn6EeghoHKyZ8NkWeu69JkNCRW6gAmPfGr/+Z5C0+fv75GtlQrM4XgsPj2B
UGeBS+Kuh/TLg7OI3SSOOLuMag25nIePA6ELaMxZFx/aJPFGH6wVwz/deffdKsiS
Rr55JrWGtvijWJdAx/6w8nyBi93ymX/3mU/ttA36fe8yFPSp8vEHWpWg/gS1lJ8u
Bo5XwlBslPIIEbNaHZgF51Dx0C+Vlvt3vo5+g09W2xJuL3NIE8kQcxDJfJBrdE2q
na6iuTLSlfUh8s6onP1gJvHA9uH0vYhuV/Kj6jiJZ4F+PxPcRsRUq+AOc/F8qllR
FHJ1G+mZwbOiJeLaXPtF
=n+zh
-----END PGP SIGNATURE-----
