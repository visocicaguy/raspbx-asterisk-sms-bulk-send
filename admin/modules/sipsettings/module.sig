-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1522900191.7627
[hashes]
LICENSE = 57c8ff33c9c0cfc3ef00e650a1cc910d7ee479a8bc509f6c9209a7c2a11399d6
Natget.class.php = dfb1e90099e657d067826a087234175d93b3689b8d4aee275f053b41a2676d5b
README.md = bd45a61ecad7fce548947e88ff2fe76167799c8a5252f02d08977198156b8bed
Sipsettings.class.php = 001f6e0076ac788ba77abe07f3bb086c1e06de5b93e1bfec7e2564077a4d27ff
assets/css/sipsettings.css = c2a89a9e28f3c47f744270ab321e7f7b1b890cd49ac6428b4bf2457926bbb282
assets/images/arrow_up_down.png = ca035ec6f9a1a2e3fcda0888c131bdf7038fbb32e3260ae38f20a9b7415708d7
assets/js/sipsettings.js = 079d01302a4be780100f7593efd48a599be9e52ec893fe5b6db14294d1570028
chanpj.page.php = 0e334005705014a3d9ad69e965dcaa8ed97f2dab8e36148610f6d25c89812094
chansip.page.php = 903b5c65ec77276ef1f88720adf167c742818c9c0012f266e4847e1aac442d88
functions.inc.php = 68640f5ade8b293f53f5da0c00f2b701ca6982ab7f8e636f7f7ac6dd7852371f
general.page.php = 173da655d7baf49c24b8ab966066a7eec97eb0f8393f43b642ca642366da0a7d
install.php = e292e8af91c1c4c38c6f68a35358a23fa2bab68d9c02ce29e9d1589886bee021
module.xml = 4cae6d4e92ce8a8c6b1315b825cef3216f1c68de51b586428d220fd86655df2b
page.sipsettings.php = 6d1dcece260f4f7e6075f7a18a9a1ccfb27d82a7844d5a872a270e0b8ed25b0f
uninstall.php = dcadcea18f16920aed58caa1c34d40999f1f97ee7f237d2ada40d4f1d8d3d858
utests/GetBindsTest.php = bb763889314b206c25af38b85d7e61cba2dcda65f24da9b6a5e483db4def7dd9
utests/NatGettest.php = e18bc84777c67d4d66e17a9edaabd5ada99ad718d9455b2d18807fefaefca9e9
utests/PjsipInterfaceTest.php = ab075726849dc02e85b1cc8da6a5537023aa1c187eab3b92a0df931cda26b9ab
utests/setuptests.php = 78df653b393c4c888e9d27cb6380910bc9709b526113851d15b19f09165e0210
utests/test_ipaddr_1 = 253a54b2cd5a5c387559adca826e5ab8569b455fbd6758a56206d2b075ff4566
utests/test_ipaddr_2 = 60aea7723459f27df40a1128baa3e88bbf8d8492997719ab0ba9dc017922b78a
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.22 (GNU/Linux)

iQIcBAEBAgAGBQJaxZzfAAoJEIbOh3Rp0urZsTIP/RsPjzuJddQ3SKyM/8J9dp0C
gJG6BqmMMZ3xhDJ+ybn1EQlKO83KAUtHq0FJqWK55JpYG8AAzaWnwp8EwW36Hfzt
aFYZfqDt7jAxf6jm3owgaQeF1UHBgXP7Nqx87HpGrLYY0PTA9IAVqnaw6rI4GyJH
kDbhaxwfaBaZ04ZiWNUklZGcvzw94Kuv0WFvQUngaZMQmeuJlv7dSIrhI7BUC9AY
CWjtvMGCy0tflB4XWSY+v32+vMgSLfQ3dq0C+OhY3+3RkuGERSM6fEqJub0wa5DX
fEKafAxs27QuAFuIF6GA3QwM9/sJYDIaCKZ8UG5MLiedeaQXGXFmskYT+xXDV4Xm
+FAzyhkhTsQMAw5uqd2M9CYQCPdqtTS2Imx9zyiO4mD1BJWTwqYSifLGE6qZBSk1
kY2d8+kPvFIHDhkEu262NMC1CwAQG+MFr1SXPumZtIo2kwqSq2NMN4tnfSaIOs7h
qG+tpu6NIDShkFluHo+tOe+Erxho2cm8SClQjaJKkhXIcXObHIfSv225NsMfqbHr
9otc3Vrf+EsjSj5pXZGWyrJJCcTTjZf3kUbRs5kwlK1Z7oUNIXD+zigBU3S0JrbT
AwJavIVXrKzZWjA453gt8E5YsehEzSPc/JIRbLOJJ3gpk6koHCapufKjxSE9qWKP
oIXGEshgIruOaHnYzth5
=q18Y
-----END PGP SIGNATURE-----
