-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1505764762.5961
[hashes]
LICENSE = 8ceb4b9ee5adedde47b31e975c1d90c73ad27b6b165a1dcd80c7c545eb65b903
README.md = 9f6e2efdb3ff04f4a4cf070059de91b3f4a20e3e67f7a0650834b4276fe9ccf1
Ringgroups.class.php = 61589672387498f803ceb12d921b04609a963354b0c738f5f93da260a67aaa63
assets/js/ringgroups.js = 64d9effd364aab73d52dd9b84fbbb8e73788cea01e6ce900066771659cbacee9
functions.inc.php = a63ec343651109bbdd12589d763547c2859b6a1dc66ee5e36f39d22d72e2a1d9
install.php = 6d795cb00c48cd8a0cb1d88a47cc932479f3d19918638d376197c2969df0f147
module.xml = 0e3d346685ea7ea85c1c5d6f794c35cadb049fbfe95308f73931d0880000a6c7
page.ringgroups.php = 2b89abe26def8546183a43439c6aa8132b13434388d6d4e0d375c826b2f0287d
uninstall.sql = ed74fe2a3dda75aeda68e6001dd91bc8095cafa86904dcfc188e4a3d67fe2447
views/advanced_form.php = c648dd06237e24791521a1c40d5edc86c897e27ccec7dcfdeea63089d3ce1067
views/bootnav.php = eca88302fced625a1cd17d032b112bdc93d0323748d4be49f9e9706c7eca7133
views/form.php = 46da3b597c75c21f3ddd99f4328a9cd6dbe44d1d6c6065224390982c6b5e3723
views/rggrid.php = 28789f64ac5c1f6b7535a9a0f391749dba90914b8442ae7671bce7ea0fa96524
views/simple_form.php = 73643219ccb31e2253e2fa608991605d18e3ff50549b1f6d8144b5a128efaee3
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJZwCWaAAoJEIbOh3Rp0urZE5UP/A7GJMKGEoEAt0Ulsk6Kt8ch
w6sxKOD47uPtThY0ZkLlL3Tzt8erHWJbctrAaUZvpl+XtnkHLDxiz8mm4dBTJHyy
/Kpd/XsQt+qz2ISW19uqohIjDx7gmDHJlhvyWJNDqX8msbe/RM8XQZXRN1FOZG6p
aRidNtY91bkc+LYos1OePBwtyPFuanbzFxoAToS61aTMIpY44TTByxRSgM5cM+9V
JT1EjamtQ294ev9VkEjRbND6PPymOmX1VNTeVKCBKtpNEJ+3t5Q6jNKBZ9ki7DOQ
UC3zIZuBzedTW9YTBxeOyJQTLUfCgEdJOt7sJGyK6z6t7hi4tWNzRq6I5Z8NZkYb
/h4EnoyORBzUsRRrBt2o7Ey8YQPrKt2yJWsK126M+GRdkVH0+2y0oFzKDa6+DTqB
MCx50m3Xgt/jHiz1rHv1MIj4s9QuTDBb566Qhb1Bb3o8ZCxBeQ0LTPaM4CB2SiAf
AuPe3k+i31o1Y6OosJDLjbm0gUfTrvaooBu/Zte6vVK3m/TxNq7wCMkqbdPYA6dy
vWUlRyHNw0AtM+HpgLCO9ya8rALLFsJDco6uzntZ/cOtSWz1rHdAx/ZQfp6EK2Um
tIbjGnxXR/j9NvWLFWAADXKzQV0mjFSivp6P9qqk2eOnbsTp53LlQzo5d1cCSL40
eS27QDPkewEHkf3kT/n/
=QzQM
-----END PGP SIGNATURE-----
