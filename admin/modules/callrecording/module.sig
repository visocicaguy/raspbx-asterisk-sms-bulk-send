-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1503442201.3513
[hashes]
Callrecording.class.php = eb74a7989343ae34f13dd3d7b415fc0cea262c14ab2c1303c4b043d76aa4e58b
LICENSE = 57c8ff33c9c0cfc3ef00e650a1cc910d7ee479a8bc509f6c9209a7c2a11399d6
README.md = ecdafd6ea4982d9896aad1ca4269c0feefe3358abc0915e65f5469e9f99b9d1c
assets/js/callrecording.js = e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
bin/one_touch_record.php = 8df0d70ff40f028f39653f39432363155a17531cd10634d10e712f77b4104ca3
bin/stoprecording.php = 8c8732276d8eeaeb7d3e38d0deceef9cdc9e26bafe1b4262316ad286b71f0239
functions.inc.php = 0fc42d53fbc6e9ff481c7d1335a532abf60a2851d27c0f141390229958d051a7
install.php = 0a1469cc798c5e246872b0b5e058c3e7f9a06f87e9b4bd4f978677e894c803f5
module.xml = 88807bab24eb44b322107ef9ea6ce4ee318b9478c6996be3ce7dac832e4d74e4
page.callrecording.php = c5cfbe84e5ca7c5464083d622425fcd967f9ca4cd991c43279db8b2e388b3157
uninstall.php = 47aec220a69739da11d319e51e7270d7866b1d279a5096f2c8d22366f988be7c
views/bootnav.php = a57e3a77cefc8009ff4236d3460a8a6d69d79ea539c452ed56087a33dc59b733
views/form.php = 80796c2c9d04dfb840ebeb9be658070fdd6bd406273db0176e3fa36f4aa948f1
views/grid.php = 19d33c0be77054784d6566bfc3723d5a233d3837226b27fb019bb4d0ace725fe
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJZnLUZAAoJEIbOh3Rp0urZ2JwP/2GgjR7Khpd8IpQNUbPnfZnD
dwk3B1KftK/ciBe90XjmlZUt9d8HwleiVNJzV45y1mN1SwC1bGUbsAd5upd4Ymw2
BdFeKAAFIW6TSOqQIIWztECOsVSII0UEQ1nTbFxsuZ8oFPkYzmS3XeqTAOHozzRm
PAC0R9TO9iReXcNd1EYuPnW007TJx4hGi8f3BNRPDCSGzGLzRpIrve0yvuo2Wc8B
d8RTpI0cge2WescNdnRUEBWf5r7fWeU+k4gaLUWvqXxhGI60CXtGUA3HSG1K6X4U
SjEzlTGLiZI8nWrTzwaLX9UV7bI+kj0DyDBcI5eczNfd5SKoZgG6pM1Q3i4aj5DU
pC/r0cF9AKhTmE71n/19b7H4Yd5e9oENfQRVQOJ4oE3Xgpd2XKODxaxQJ6kPWLpO
u6GMygvPPWx+nFUt28T0o8qBHLg5h6SO7CXwLt7cd6f/7ngHfdFCJVYvdzqjW7kQ
ZP573NMfjNkwnXbLtnb8X2RaDB1PFvzaxSi9ZUGoXvtXa/5R5zq1z9ZkN1kc5kQR
Ke8r8XtQDwt100R0Pv5XPQSrC73P2Gj5Qf2qlb4zefNnoRwclmVQNL1UzoYYM1//
9njGudGxy3zD5+2HxLh7OZPl9UEeNJGrU1e/MrNDc/agaLEeLyh/VMf9HwXBTfSA
DBQ70trne5RBKMquXGkk
=JpBn
-----END PGP SIGNATURE-----
