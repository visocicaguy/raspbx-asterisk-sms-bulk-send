-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1518481126.2801
[hashes]
Iaxsettings.class.php = 834cdc012b9bf7ff21ef21f8ae1d615cac4f168175e8e6930d9b9922e03acdc7
LICENSE = 57c8ff33c9c0cfc3ef00e650a1cc910d7ee479a8bc509f6c9209a7c2a11399d6
README.md = eb320a1e9cf70561901a15ec418f42bd5dd573eda712cbb4af739ccf0935f7c2
assets/css/iaxsettings.css = 364385348abcd28a026fef9cd24ca0e8ea5a786d5b40bb955d82a49aabc1ac25
assets/images/arrow_up_down.png = ca035ec6f9a1a2e3fcda0888c131bdf7038fbb32e3260ae38f20a9b7415708d7
assets/js/iaxsettings.js = 9629d0b701ab71657e518e337d0953d606a3a7222de2d30e12182dde5b45cc1b
functions.inc.php = e4522998f48b65244c225489e93c2c998b45599405f65a628e998d1976efed67
install.php = a2b34b94908a7ab84124759a8c5e2a359ca2d35eacc126615adc0348681ca708
module.xml = 332f93179a38c084be08cabf1706f67f00fc673b2b490c82148b929bf3906725
page.iaxsettings.php = 4eaea2e208fd601305c96c1b3c70af88a5d05abcd73f5bf66c022b3d944fced3
uninstall.php = 022a23f0d1ef56c73041debae15293b750767a81745f9ba8d1ec5e1957af3d08
views/form.php = 82a585ac68dcf691bd8ada0085bcb35745029c2f0717126e9f6f4a57fbcd822e
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJagi7mAAoJEIbOh3Rp0urZVswP/3xddwKJFhntGHLJ1BDiIo8E
kxZkDv5s8vu9XkViOWxDurszHaRifBvwqrryUiYzWbHNZaVEJPaUMODgqRCgaD2p
nttFipjHF5NioK+y/YnMTPNN+UoH2XlspeHLebzspc4c8bYqVBK1T1Ui6laBxV4d
7DCd+Sdim0QiZ7bMqPKbU93G8innHL+FJMfpXYm8ugFMLvQ+cMl3j4qSSSWOoGlc
xR891lak0dePUZ+m1xg6sVBifKFxepUr9HrQueBZaOoDpWTTefHueiVdznRvNLhq
0nYGCs3UaYL34BbTondw8/V47HVOYpo0rAQd0KrCpS7q1y2s3oSs2J1nsZ5G5tb1
QVYJUjzgF8OfKdobGpKPfGlXUYK7WDWTYS63VLLCzXexYPffO70bdSDPKQUxvG5k
KI/s6wy/+CJEX4TfvSnKXaz5+vu4lax7uv8LFeYbA3PTkMcwXvFBNvlTnrHe95MQ
3rIbaX50j7X+7Wh+yYE8yvd/n0hZIhxnU8EIcps7U/Pq3OU7A8UYZ86QSWyOTzIM
FOCrS0/r1JaL0U5U+hG1g6FCZIrb0z3lGCRS+C+Kr4WAYdB+8/ZpqMktzYZnUJYW
nHknqELeMeY9uH+HJ9DwSvl19EFMtoan9rMES3QsGWRwhKegmi597bAwrYuqkuR4
ylq3ucU1Bl3zdeMNnrSz
=Jgpm
-----END PGP SIGNATURE-----
