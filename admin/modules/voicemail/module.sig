-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1520455177.21
[hashes]
LICENSE = 8ceb4b9ee5adedde47b31e975c1d90c73ad27b6b165a1dcd80c7c545eb65b903
README.md = b58205f4059ac66c30d80b15d40e763caab9faf7c1446da4a0e0bdf8f900bbe8
Vmx.class.php = 5e08e2dfaebab2909ffdd57280524275fc70b02dddb34890f5d308d1dfbb8a0d
Voicemail.class.php = 4a756daa43588fa3c045b96f421a63f9284c88f3c8188c75a02f367551a3d74b
assets/js/voicemail.js = 88de75d28ae4a0481bd7992aac88e902bb7919950d577ccb5a4032bde9d240b7
assets/less/voicemail.less = cc1114748fb29621574dacf105b4890d7899d9f32b42e0cd5a5b83beb75ec3d4
functions.inc.php = 0af52a8c4e919e20dd719362a0aae5ee164d4a44b08cd433950037323bd2d405
install.php = bb8a4d1556c4df6f37cdd965c314608c14fb2973eede7701fc2da5b3bcc8fdb8
module.xml = 4d5f9e43c96b17e1b78923d5447bab4ab671132e6967098bc33a84ac305663dc
page.voicemail.php = e0faedc0e9043b6a293c5622799b3dbe0fe81e86f6f787cde518764cf7fa5704
ucp/Voicemail.class.php = c574f63defa071543c51da6906bd61a6bb2531e20611b3f4cda5f5aa882d3655
ucp/assets/images/browser_download.png = 2177f987023f057a3688f54d303f4c5c8a52ea9d8cce0cd72e41418881a24b40
ucp/assets/images/button_pause_green.png = d440607238f531b1596de71f290b14a99b7de57e59c3ef975e779fe72a7697ea
ucp/assets/images/button_play_green.png = 1e50e11a2b1a607085146b350fe82e7088fa7bf857ef62965b28f35057b3861f
ucp/assets/images/jplayer.blue.monday.jpg = ab94479128a0f02fd2facb680a307e00a8b8bf2a94adb0399b1fd984e49528b5
ucp/assets/images/jplayer.blue.monday.seeking.gif = 00d752a8e28100f2183e5e837b99c35840b5b9e5a35a62d3551670258f58f705
ucp/assets/images/mail.png = b80639f0c236b069b3f087fa1e8158de691613a8f34a40da2d8f877134051a5e
ucp/assets/images/pause_control.png = 0f5cbb7c5782c077a0cf5aa96632454cfaadfc3b7663ed6a8b46a9ee56cc9eab
ucp/assets/images/play_control.png = 6bccd6a9d39f4417ebcaaebdf4023e2edbe11b47e707107916d88f8b1892b0ca
ucp/assets/images/play_controls.png = 3ad8aa384da60352d41d5fdcbc3382f5e40890516586942ba445b290efd08d16
ucp/assets/images/settings.png = 216f6c391ed93f3ac7f24b74f0f25d9ec4615c1d3dc881f7f1e8bb3675cc7915
ucp/assets/images/trash.png = d16c6177e1fac7c64bd1e7b768b134d61d6176911c266b2c3793cdecfcdc6f43
ucp/assets/js/global.js = b9eb125c32dfbf9bf67dad722e916a5b59f3c96fb2746d91843be86a16caf00d
ucp/assets/less/bootstrap.less = 140e25561fe3fb0b1e35159830d7cd8973c6e4dafc1fb98fe93f51ea6432a3a7
ucp/views/greetings.php = 2eee9baa785f65ef5d07f96417b4340c1e35c942ee18e83b0cf6ca6da416ed7f
ucp/views/settings.php = 18cb6a98e43c13f03cee413de4ab3aff5e5057c5bd8767505fa11ba4a65874d5
ucp/views/vmsettings.php = 762c869b6ef023d755ec66539b608233b6f6e6004be38e407f1b9f371030ca74
ucp/views/vmx.php = cb792cd53a6094d8edc1aaae529934ea89d8558cba6f2653c582566a81e74c8d
ucp/views/widget.php = 8237dfdd1482858f33b4ba312b92437395902f0ba92c122bb66efcc9f909ab33
views/dialplan.php = 9f9b087444e02dd5be8a83d64767cca97cf46c9c27ad8e649e4b1af279a2bd19
views/footer.php = c250f37dac6c3550b405051f5fc7fab7eec6ac97ebfe22bc23eb35bb695e5949
views/header.php = 86282669c1e9a1cdc231a223e32a3c5e7137747b4180b8123075db0ecc24e342
views/nav.php = c1749d1c49e31f7c27b125c0892219f19fec02232404708d9afb2ea41c58e9a4
views/quickCreate.php = d54d29c9d6c1a5e436249ec1d5c89e9e28fdafb8660096bb92b575dde6bf5e58
views/settings.php = 01e7534f2b79ca4dc23b4ea1c44f992738d550b6550e4924bd80a9ecb51007a6
views/ssettings.php = 3a134bd1c8c82af97a33e6b7d8834631e8a899d4b7935cf2774795588651d45b
views/tz.php = 51f09b07d68e2aa9946ddb2aae53c9faa2c03945d0b905ba352d30bd6f04d68e
views/ucp_config.php = 510a3a6519861962661ffe0e9e35209d8e1afeee53d8565432e713130b439434
views/usage.php = 4ffd005d4119cabfb5fd69c42a0235fa7f3c97f9e4274c2af9d53a6b62b76bee
views/usage_system.php = 1f8c6464789000bb0b4e100cb8b3b19195618573a60add1d589e8be77132dea0
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.22 (GNU/Linux)

iQIcBAEBAgAGBQJaoE4JAAoJEIbOh3Rp0urZg60QALJKP1xHXJxE6DtY+Y5qetdE
5+rwaQAdA70Dv0rsuSbEAnUO+OomYl4sm32hNmdC8aVSXDyMqiaDJfus4AZxKor5
U9HRpn1miNMIfaUnBT6PEguX0/rdo8awytOsE4Mj+vko+277Kl+4y475y2ODD0he
Ficb4TegJDDymR5sKzJXkRdvrSoYLZVDE5twIwoaSFqYoliuqKnET/D0O7OIFdu6
oGuwBwF4ayg0/9ghGshnh8tM3WKzu0iyNy1QhhhzuGrlQfBWFwOLV1KuK2t3lPsl
7W7GgyFfy7SxYZwcyq/lJ2OiCo2eixzbwndICVadF71g+LXY8hI+r8a1yThbED0M
pERJNhcYRkR1j1lenKtTx67Oh3gS5cu0yljsnAxHUGWG7TZmYyhSkSWmYU1jOGoo
SXWQZV/caIP9MZK9BWI/J+xPx1Hs/Uw2eva3eCPnlGAVa5dIdNtrtqZIcPXCwpRo
B4aTp57ISWtI/hIluG/w9YjyYlDeqvNfW+0LNTL11sdHWg+lQ6pPWFhjRFajCH09
SJY0jqTgB4VArtLZM6o06BjiZbG0yGzIGcuJe4jq0bpxjJXZkgoHpPWcl7i3iJ8I
1Ju+CDIhtoNxR3kMbXE3VumopX36GLzNGlwRfawj4kTyo5xvRKTatMANX9m1qBrA
6ry2KeHzAnNWN9RlbyVL
=mbd8
-----END PGP SIGNATURE-----
