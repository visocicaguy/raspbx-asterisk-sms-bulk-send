-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

;################################################
;#        FreePBX Module Signature File         #
;################################################
;# Do not alter the contents of this file!  If  #
;# this file is tampered with, the module will  #
;# fail validation and be marked as invalid!    #
;################################################

[config]
version=1
hash=sha256
signedwith=86CE877469D2EAD9
signedby='FreePBX Mirror 1 (Module Signing - 2014/2015) <security@freepbx.org>'
repo=standard
timestamp=1505935076.0182
[hashes]
Ivr.class.php = e869af0db5dd00a801b4bbd8be91055323afe3cd4d8b278afba668ee3b9f0286
LICENSE = 8ceb4b9ee5adedde47b31e975c1d90c73ad27b6b165a1dcd80c7c545eb65b903
README.md = e83ef8331ff0070883a7d91b3d0b3e4cf63706811d5c7816d8f5b07232371f66
assets/images/add.png = c06a52df3361df380a02a45159a0858d6f7cd8cbc3f71ff732a65d6c25ea6af6
assets/images/queue_link.png = 9379b0e7a6e72d531a0626d485f980b52d41852bc99340d45289508ed80eaed5
assets/js/ivr.js = db0da3b43d360a6670e084df2164a8e6f444109a038f4e60f28fe13b966f0929
assets/less/ivr.less = 2cc98a2827137f7b5bdc5851902dd62b2b1d7f6942340727b031af82947cd715
functions.inc.php = f6110e9f3bcdc167ab862590af91bba7adfc904c794e4631677fde586d8776ef
install.php = 2f660ab48ae5c0ec89892e3cf9fd4986adc876b2196c33070c2a076e73c063cd
module.xml = 72cdfda47e866af84328f05b557b6d8684ae27e6fe4103c188c9595b77bd4e5e
page.ivr.php = 0c823a6d27a3fde9da528993140459907e4b35adab311fae1d15742a5265f4db
sounds/en/no-valid-responce-pls-try-again.wav = 6980208bbe3b869ae6713af0ae4ebd3735952f0da39c1672bfd61a6decc2ab2f
sounds/en/no-valid-responce-transfering.wav = 2fd1f6f7c516e0d2d44d4e8e6a8c43fb628047c51dc77a38ff9f7fb3ba746a02
uninstall.php = 795d94ecf12f1a7edba0f2955f9d0cb22dd2f3380d6955e40224d8ab03cf6039
views/advanced_form.php = e10c5e9dc04ec79f0b94bb5ea46d0566e8fb552e3c145eb0c34eb0c78abd4c48
views/entries.php = edd09faa5a4b40c0cd5fcae997f4c25ef8e17b337af92e0b1937169fcae56dcb
views/form.php = 4223b33eec6abe230701eb1958c49c73954552afef61375333e7bde534a78642
views/rnav.php = c37ad833db67ad9cc41f42293eb8e3b4463c282e254f92fa43eec5ced23ffecd
views/simple_form.php = 1c59c4d4edb7c94309dddd678234af2acb87acfbd258700d0e8545d287a2e301
;# End
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iQIcBAEBAgAGBQJZwr7kAAoJEIbOh3Rp0urZ3c4QALqV5spQare+MtMWV7mzpPLF
NiS/rICqMitfrmOp6Kd5xkPjA30y0/pinhvri85fMXKEr/16klEfvaA+mXyWhhVS
ZWjkTsv0KcfqavWAjqz54088hALDRBDXUrtzT/ZWaumXKQ/r4US5DcInZpUfEwub
PYkPDtSpAXln7hN5j03Nh/oBZl2JFnvhd+P0OLLfE1Qie2Fb9e2skk/nQ4dYjGf2
BAiCOUSMSbMM7mTn9wNBsccYxRY/CxHU+knlb6tM8UUZNFw0EW0Cuk/9ddXUicdb
L81I6oUB08DcWGQYy+xrFi2Hb9y6mws+VhxTGCdA4zdxLTATG6zpUBm5BZjPlbcK
mbZx0erT+zI+TYR2Pkf1Msiewi6J+WpHSNaW4iMl1HMVUKVIQ+hoDnYAKiSqupra
RaLSPf5abxC76SB7vxaPvYOdxux/K9wNGgWgfF0ABPtt7AVoeaFpZEKXYXqqYeO9
JsTCKxum86TBJwZn5yhZnjRldoM47Dl3UEydgXhgIBAAOLIrBvpLMbVy1ECiAtdZ
FHaXcqTvYi2ukzzFNxHM3is6DvYWlGCcgZ2Gy5Sl4yHEPI9rI1l7FsmUIiQ5m3lA
gWRhRV8lv7EoJEsbc/h5JOKMYYh+koRjl7deQy7ra3Qby3mz81hXplR6Rx1sb9sr
6U1LVQcY6BKhXn0Sqcek
=NVoE
-----END PGP SIGNATURE-----
