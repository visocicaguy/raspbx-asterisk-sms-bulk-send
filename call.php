<?php
#set timezone
date_default_timezone_set('Europe/Sarajevo');

#check SIP registry
$output = shell_exec("asterisk -rx 'sip show registry'");
echo "<pre>$output</pre>";

if (strpos($output, 'Registered') == false) :
echo "Error: SIP not registered";
http_response_code(500);
exit() ;
endif ;

$strTrunk = "Channel: SIP/deverto/";
$number=$_REQUEST['number'];
$strChannel = $strTrunk . $number;
$strApplication="Application: Playback";
$language=strtolower($_REQUEST['language']);

if ($language == null) :
$language = "ba";
endif;

$strData="Data: /var/lib/asterisk/sounds/zoblak";
$announcement=strtolower($_REQUEST['announcement']);

if ($announcement == null) :
$announcement = "zoblak-farm-alarm";
endif;

$strData = $strData . "/" . $language . "/" .$announcement;
$pos=strpos ($number,"local");

if ($number == null) :
http_response_code(500);
exit() ;
endif ;

if ($pos===false) :

$file = "calls_to_be_placed/" . $number . "-" . date(DATE_ATOM) . ".call";
$fileContent = $strChannel  . "\n" . $strApplication . "\n" . $strData ."\n";
file_put_contents($file, $fileContent);
echo $fileContent;
shell_exec("mv $file /var/spool/asterisk/outgoing");
echo nl2br("\n\nChannel $strChannel should be calling $number.\r\n");
sleep(2);

#check SIP channels
$output = shell_exec("asterisk -rx 'sip show channels'");
echo "<pre>$output</pre>";

if (strpos($output, $number) == true && strpos($output, "Tx:") == true) :
echo "OK: Call to $number is underway...";
http_response_code(200);
else :
echo "Error: Call is not verified!";
http_response_code(500);
endif ;

else :
http_response_code(500);
exit() ;
endif ;
?>
