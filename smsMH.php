<?php
#set timezone
date_default_timezone_set('Europe/Sarajevo');

$number=$_REQUEST['number'];
$announcement = "Postovani, cast nam je pozvati vas na predizborni skup u Mostaru, 19.9.2018. u 20 h u XXXX";

if (($number == null) || strlen($number)<10 || !is_numeric($number)) :
	echo "non valid number";
	http_response_code(500);
	exit() ;
endif ;

#sens SMS
$output = shell_exec("asterisk -rx 'dongle sms dongle0 $number $announcement'");

if (strpos($output, "SMS queued for send with") == true) :
	echo "OK: Sms sent";
	http_response_code(200);
else :
	echo "Error: Sms not sent!";
	http_response_code(500);
endif ;

echo $output;

?>
