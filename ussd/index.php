<?php
/*********************************************************************
 *  Chan_Dongle ussd Script v.0.1
 *  for The Raspberry Astertisk and BBB Asterisk
 *
 *   Author: Naveed Najam
 *    Email: naveed.najam@gmail.com
 *
 *********************************************************************/
?>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
      <link rel="stylesheet" href="style.css" type="text/css">
      <title>USSD for Asterisk</title>
      <script src="ajax_ussd.js"></script>
   </head>
   <body>
    <a href="./index.php"><h2 align="center">USSD for Asterisk</h2></a>

    <h3>USSD Code:</h3>
    <font size="-2">(Format: *XXX*XXX# or *XXX# )</font>
      <form name="ussdform">
        <input id="phonenumbers" name="phonenumbers" type="text" size="25" value=""></input>
        <button type="button" onclick="sendUSSD()">Send</button>
      </form>

        <h4>Command Status:</h4>
	<div id="commandStatus">
        <p class="status"> Command execution result will be shown here...</p>
        </div>

       <h4>Network Response:</h4>
       <div id="Response">
       <p class="response">USSD respone from network  will be shown here...</p> 
       </div>
   </body>
</html>
