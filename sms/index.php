<?php
/*********************************************************************
 *  Chan_Dongle SMS Script v.0.01
 *  for The Raspberry Asterisk
 *
 *   Author: Troy Nahrwold
 *    Email: Troy(at)eternalworks(dot)com
 *  Company: Eternal Works
 *  Website: www.eternalworks.com
 *
 *  Disclaimer:  
 *   This product is solely a private production of the above named 
 *   author, and is neither endorsed nor supported by Eternal Works.
 *   Although this product has been thuroughly tested, it is 
 *   distributed AS IS, and the author assumes no liability for any 
 *   damages this script may cause to your system.  The author 
 *   has provided full source code and encourages you to review the
 *   source code to determine any effects it may have on your system.
 *
 *   (c) Copyright 2011, Troy A Nahrwold, Eternal Works, LLC.  
 *       All Rights Reserved.
 *
 *********************************************************************/

$dongle = "dongle sms dongle0 ";
$ini = "'";
$password = 'ab9d899ebd97ba7d21259dc8031700f75b55cf56';

session_start();
if (!isset($_SESSION['loggedIn'])) {
    $_SESSION['loggedIn'] = false;
}

if (isset($_POST['password'])) {
    if (sha1($_POST['password']) == $password) {
        $_SESSION['loggedIn'] = true;
    } else {
        die ('Incorrect password');
    }
} 

if (!$_SESSION['loggedIn']): ?>

<html><head><title>SMS Messaging for Asterisk - Login</title>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
  <body>
    <p>Please login:</p>
    <form method="post">
      Password: <input type="password" name="password"> <br />
      <input type="submit" name="submit" value="Login">
    </form>
  </body>
</html>

<?php
exit();
endif;

if(isset($_REQUEST['phonenumbers']) && !empty($_REQUEST['phonenumbers']) && !empty($_REQUEST['message']))
 {
   $message           = substr($_REQUEST['message'],0,160);
   $phonenumberarray1 = explode(' ',$_REQUEST['phonenumbers']);
   $phonenumberarray2 = array();
   $phonenumberarray3 = array();

   foreach ($phonenumberarray1 as $phonenumber)
   {
     $phonenumberarray2 = array_merge($phonenumberarray2,explode(',',$phonenumber));
   }
   foreach ($phonenumberarray2 as $phonenumber)
   {
     $phonenumberarray3 = array_merge($phonenumberarray3,explode("\n",$phonenumber));
   }

   $output = "Message: $message<br><br>\n";
   foreach ($phonenumberarray3 as $phonenumber)
   {
      $runcommand = '/usr/sbin/asterisk -rx' . $ini . $dongle . $phonenumber . " " . $message . $ini;
     $output .= "Sending message to: $phonenumber<br>\n";
     exec($runcommand);
   }
 }
?>
    <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
      <title>SMS Messaging for Asterisk</title>
    <link rel="stylesheet" href="style.css" type="text/css">

    </head>
    <body bgcolor="#84b0fd" text="#030303" link="#9abcde">

    <a href="./index.php"><h2 align="center">SMS Messaging for Asterisk</h2></a>
  
    <table border="0" cellspacing="0" cellpadding="1" width="600" bgcolor="#ffffff" align="center">
      <tr>
        <td>
          <table border="0" cellspacing="0" cellpadding="3" width="100%" bgcolor="#ffffff" align="center">
            <tr bgcolor="#abcdef">
             <td><b><?php echo $output; ?></b></td>
            </tr>
  <tr><form action="index.php" method="post">
    <p><b>Phone Numbers:</b> <br><font size="-2">(Format: NXXNXXXXXX Separate numbers with commas or newline)</font></p>
    <textarea id="phonenumbers" name="phonenumbers"></textarea>
    <p><b>Message:</b> <br> <font size="-2">(Message will be truncated to 160 characters) </font></p>
    <textarea id="message" name="message" size="160"></textarea><br /><br />
    <button type="submit">Send Message</button><br /><br />
  </form></tr>
          </table>

        </dd>
      <p></td>

            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td bgcolor="#eeeeee"><a href="javascript:history.back()">Send Another SMS Message</a></td>
            </tr>
          </table>

        </td>
      </tr>
    </table>
  
    <p>
    <table border="0" cellspacing="0" cellpadding="1" width="400" bgcolor="#000000" align="center">
      <tr>
        <td>
          <table border="0" cellspacing="0" cellpadding="3" width="100%" bgcolor="#eeeeee" align="center">
            <tr>

              <td align="center">
                Created by <a href="http://www.eternalworks.com">Troy Nahrwold</a>. change by <a href="http://www.dmtg.org">DMTG.org</a>. Optimized for <a href="http://raspberry-asterisk.org/">Asterisk in Raspberry Pi</a>.
              </td>
            </tr>
          </table>
        </td>

      </tr>
    </table>
  
    </body>
    </html>
