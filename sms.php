<?php
#set timezone
date_default_timezone_set('Europe/Sarajevo');

$number=$_REQUEST['number'];
$language=strtolower($_REQUEST['language']);

if ($language == null) :
$language = "ba";
endif;

$announcement=strtolower($_REQUEST['announcement']);

if ($announcement <> null) :
 $announcement = $announcement . " - ";
endif;

if ($language == "ba") :
 $announcement = $announcement . "Zoblak Farm Alarm - Provjerite HITNO!! http://agrar.zoblak.com";
else :
 $announcement = $announcement . "Zoblak Farm Alarm - Check NOW!! http://agrar.zoblak.com";
endif;

if (($number == null) || strlen($number)<10 || !is_numeric($number)) :
echo "non valid number";
http_response_code(500);
exit() ;
endif ;

#sens SMS
$output = shell_exec("asterisk -rx 'dongle sms dongle0 $number $announcement'");

if (strpos($output, "SMS queued for send with") == true) :
echo "OK: Sms sent";
http_response_code(200);
else :
echo "Error: Sms not sent!";
http_response_code(500);
endif ;

echo $output;

?>
